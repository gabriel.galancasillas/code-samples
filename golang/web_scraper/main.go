package main

import (
	"log"
	"os"
	tginputadapter "scraper/adapter/input/telegram"
	scraperoutputadapter "scraper/adapter/output/scraper"
	sqliteoutputadapter "scraper/adapter/output/sqlite"
	tgoutputadapter "scraper/adapter/output/telegram"
	"scraper/config"
	"scraper/core/usecases"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load("PRO.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	bot := config.InitConf()

	scraperRepo := scraperoutputadapter.NewScraperOutputAdapter()
	sqliteRepo := sqliteoutputadapter.NewSqliteOutputAdapter()
	telegramRepo := tgoutputadapter.NewTelegramOutputAdapter()
	usecases := usecases.NewPropertyUseCase(scraperRepo, sqliteRepo, telegramRepo)
	tia := tginputadapter.NewTelegramInputAdapter(usecases)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message != nil || update.SentFrom().UserName == os.Getenv("USERNAME") { // If we got a message
			switch update.Message.Text {
			case "/feed":
				tia.GetFeed(update)
			case "/scrap":
				tia.ScrapWeb(update)
			}
		}
	}
}
