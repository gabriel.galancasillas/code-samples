package tgoutputadapter

import (
	"fmt"
	"log"
	"os"
	"scraper/core/domain"
	"scraper/core/ports"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type TelegramOutputAdapter struct {
	bot *tgbotapi.BotAPI
}

func NewTelegramOutputAdapter() ports.TelegramRepository {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_BOT_API_KEY"))
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	return &TelegramOutputAdapter{
		bot: bot,
	}
}

func (a *TelegramOutputAdapter) SendProperties(properties []domain.Property, chatID int64) error {
	var textContent string
	if len(properties) == 0 {
		a.SendMessage("No hay pisos", chatID)
	}

	for i := range properties {
		textContent = ""
		if properties[i].Id == "" {
			continue
		}
		textContent = fmt.Sprintf("*%v*\n%v\n[link](%v)", properties[i].Name, properties[i].Price, properties[i].Url)
		msg := tgbotapi.NewMessage(chatID, SanitizeTelegramMessage(textContent))
		msg.ParseMode = "MarkdownV2"

		a.bot.Send(msg)
	}

	fmt.Println(textContent)
	return nil
}

func (a *TelegramOutputAdapter) SendMessage(message string, chatID int64) error {
	log.Println("Sending message", message)
	msg := tgbotapi.NewMessage(chatID, SanitizeTelegramMessage(message))

	_, err := a.bot.Send(msg)

	return err
}

func SanitizeTelegramMessage(text string) string {
	return strings.ReplaceAll(strings.ReplaceAll(text, ".", "\\."), "-", "\\-")
}
