package sqliteoutputadapter

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"scraper/core/domain"
	"scraper/core/ports"
	"time"
)

type SqliteOutputAdapter struct {
	db *sql.DB
}

func NewSqliteOutputAdapter() ports.SqliteRepository {
	db, err := sql.Open("sqlite", "assets/db.sqlite")

	if err != nil {
		log.Fatal(err)
	}

	if _, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS properties (id VARCHAR(64) PRIMARY KEY, url VARCHAR(64), name VARCHAR(64), price VARCHAR(64), image VARCHAR(64), timestamp DATE);
	`); err != nil {
		log.Println("Error has occured")
		log.Println(err)
		return nil
	}

	return &SqliteOutputAdapter{
		db: db,
	}
}

func (a *SqliteOutputAdapter) GetAll() ([]domain.Property, error) {
	response := make([]domain.Property, 0, 10)

	rows, _ := a.db.Query("SELECT url, id, name, price, image FROM properties ORDER BY timestamp DESC")

	for rows.Next() {
		fmt.Println("Row:")

		var row domain.Property
		if err := rows.Scan(&row.Url, &row.Id, &row.Name, &row.Price, &row.Image); err != nil {
			log.Println(err)
		}

		fmt.Println(row)
		response = append(response, row)
	}
	log.Println("Sending response with ", len(response))
	return response, nil
}

func (a *SqliteOutputAdapter) GetById(id string) (domain.Property, error) {
	var property domain.Property
	row := a.db.QueryRow("SELECT url, id, name, price, image FROM properties WHERE id = ?", id)
	if err := row.Scan(&property.Url, &property.Id, &property.Name, &property.Price, &property.Image); err != nil {
		return domain.Property{}, errors.New("property not found")
	}

	fmt.Println(property)
	return property, nil
}

func (a *SqliteOutputAdapter) Insert(property domain.Property) (int64, error) {
	statement, _ := a.db.Prepare(`INSERT INTO properties (id, url, name, price, image, timestamp) values(?,?,?,?,?,?)`)
	res, err := statement.Exec(property.Id, property.Url, property.Name, property.Price, property.Image, time.Now())
	if err != nil {
		return 0, errors.New("couldn't insert property")
	}

	lastInsertId, err := res.LastInsertId()
	if err != nil {
		log.Panic("Inserted but error getting Id")
	}
	return lastInsertId, nil

}

func (a *SqliteOutputAdapter) Update(property domain.Property) (int64, error) {
	statement, _ := a.db.Prepare(`
			UPDATE properties
			SET url = ?, name = ?, price = ?, image = ?, timestamp = ?
			WHERE id = ?`)
	result, err := statement.Exec(property.Url, property.Name, property.Price, property.Image, property.Id, time.Now())
	if err != nil {
		log.Printf("Error updating %s\n", property.Id)
	}

	return result.LastInsertId()
}
