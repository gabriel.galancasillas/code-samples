package scraperoutputadapter

import (
	"fmt"
	"log"
	"scraper/core/domain"
	"scraper/core/ports"

	"github.com/gocolly/colly"
)

type ScraperOutputAdapter struct {
}

func NewScraperOutputAdapter() ports.ScraperRepository {
	return &ScraperOutputAdapter{}
}

func (a *ScraperOutputAdapter) ScrapeWeb(properties_channel chan domain.Property) error {

	// initialize the slice of structs that will contain the scraped data
	var properties []domain.Property

	// instantiate a new collector object
	c := colly.NewCollector(
		colly.AllowedDomains("lunainmo.es", "localhost:8080"),
	)

	// called before an HTTP request is triggered
	c.OnRequest(func(r *colly.Request) {
		r.Headers.Set("User-Agent", "1 Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148")
		fmt.Println("Visiting: ", r.URL, r.Headers)
	})

	// triggered when the scraper encounters an error
	c.OnError(func(_ *colly.Response, err error) {
		fmt.Println("Something went wrong: ", err)
	})

	// OnHTML callback
	c.OnHTML(".col-md-4.listing_wrapper", func(e *colly.HTMLElement) {

		// initialize a new Product instance
		property := domain.Property{}

		isForSale := e.ChildText(".ribbon-inside.venta")

		if isForSale == "" {
			fmt.Println("Property already sold. Ignoring")
			return
		}

		property.Id = e.Attr("data-listid")
		property.Url = e.ChildAttr("a", "href")
		property.Image = e.ChildAttr(".listing-unit-img-wrapper img", "data-original")
		property.Price = e.ChildText(".listing_unit_price_wrapper")
		property.Area = e.ChildText(".infosize")
		property.Name = e.ChildText("h4 > a")

		// add the product instance with scraped data to the list of properties
		properties = append(properties, property)
	})

	c.OnScraped(func(r *colly.Response) {
		log.Printf("Finish scraping %v properties in LunaInmo\n", len(properties))

		// write each product as a CSV row
		for _, product := range properties {
			log.Println("Sending property scraped through channel")
			properties_channel <- product
		}
		defer close(properties_channel)
	})

	// open the target URL
	// c.Visit("https://lunainmo.es/buscador-avanzado?adv_location=Getafe&filter_search_type%5B%5D=viviendas&filter_search_action%5B%5D=venta&is2=1&submit=BUSCAR+INMUEBLES")
	c.Visit("https://lunainmo.es/buscador-avanzado?filter_search_action%5B%5D=venta&filter_search_type%5B%5D=viviendas&advanced_city=getafe&advanced_area=&advanced_rooms=&advanced_bath=&price_low=&price_max=260000")
	// c.Visit("http://localhost:8080/lunainmo.html")

	return nil
}
