package tginputadapter

import (
	"scraper/core/ports"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type TelegramInputAdapter struct {
	usecases ports.PropertyUseCase
}

func NewTelegramInputAdapter(
	usecases ports.PropertyUseCase,
) *TelegramInputAdapter {
	return &TelegramInputAdapter{
		usecases: usecases,
	}
}

/* TODO: Is it ok to have multple logic in here? */
func (a *TelegramInputAdapter) GetFeed(update tgbotapi.Update) string {
	a.usecases.GetProperties(update.Message.Chat.ID)
	return "banana"

}

func (a *TelegramInputAdapter) ScrapWeb(update tgbotapi.Update) string {
	a.usecases.UpdateData(update.Message.Chat.ID)
	return "banana"

}
