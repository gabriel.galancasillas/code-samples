package httpinputadapter

import (
	"scraper/core/ports"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type HttpInputAdapter struct {
	usecases ports.PropertyUseCase
}

func NewHttpInputAdapter(
	usecases ports.PropertyUseCase,
) *HttpInputAdapter {
	return &HttpInputAdapter{
		usecases: usecases,
	}
}

/* TODO: Is it ok to have multple logic in here? */
func (a *HttpInputAdapter) GetFeed(update tgbotapi.Update) string {
	a.usecases.GetProperties(update.Message.Chat.ID)
	return "banana"

}

func (a *HttpInputAdapter) ScrapWeb(update tgbotapi.Update) string {
	a.usecases.UpdateData(update)
	return "banana"

}
