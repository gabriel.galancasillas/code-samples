# Real state property scraper

## File structure
web_scraper/
├── core/                          # Core business logic (domain layer)
│   ├── domain/                    # Core entities
│   │   └── property.go
│   ├── ports/                     # Interfaces to "access" to domain
│   │   └── property_repository.go
│   └── service/                   # Application services (use cases)
│       └── property_service.go
├── adapter/                       # Adapters (implementations of ports)
│   ├── input/                     # Data that comes in to the domain
│   │   ├── scraper                # Gets properties from web
│   │   └── telegram
│   └── output/                    # Data that goes out of the domain
│       └── sqlite
├── config/                        # Configuration files
│       └── config.go              # Configuration setup
├── pkg/                           # Shared utilities or libraries
│   └── utils/                     # Utility functions
│       └── logger.go              # Logging utility
├── scraper.go                     # Go module file
└── go.mod                         # Go module file

## TODO
- [ ] Improve scrape
- [ ] Add more realstates
- [ ] Expose search urls

## Linux service config

`systemctl status linux-service.service`
or start or stop

To update service:
`systemctl daemon-reload`