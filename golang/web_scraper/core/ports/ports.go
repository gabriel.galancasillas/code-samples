package ports

import (
	"scraper/core/domain"
)

/* Inputs */
type PropertyUseCase interface {
	UpdateData(chatID int64) error
	GetProperties(chatID int64) (domain.Property, error)
}

/*
Outputs

TODO: Is it ok to have multiple repos per domain?
*/
type SqliteRepository interface {
	GetAll() ([]domain.Property, error)
	GetById(id string) (domain.Property, error)
	Insert(domain.Property) (int64, error)
	Update(domain.Property) (int64, error)
}

type ScraperRepository interface {
	ScrapeWeb(property_channel chan domain.Property) error
}

type TelegramRepository interface {
	SendProperties([]domain.Property, int64) error
	SendMessage(message string, chatID int64) error
}
