package usecases

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"scraper/core/domain"

	"github.com/gocolly/colly"
)

func ScraperGrupoTizona() {
	// initialize the slice of structs that will contain the scraped data
	var products []domain.Property

	// instantiate a new collector object
	c := colly.NewCollector(
		colly.AllowedDomains("grupotizona.es"),
	)

	// called before an HTTP request is triggered
	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting: ", r.URL)
	})

	// triggered when the scraper encounters an error
	c.OnError(func(_ *colly.Response, err error) {
		fmt.Println("Something went wrong: ", err)
	})
	// OnHTML callback
	c.OnHTML(".property-item-wrapper", func(e *colly.HTMLElement) {

		// initialize a new Product instance
		product := domain.Property{}

		fmt.Println("Element found")

		// scrape the target data
		product.Url = e.Attr("data-rh-id")
		// e.Attr()

		// add the product instance with scraped data to the list of products
		products = append(products, product)
	})

	c.OnScraped(func(r *colly.Response) {

		// open the CSV file
		file, err := os.Create("products.csv")
		if err != nil {
			log.Fatalln("Failed to create output CSV file", err)
		}
		defer file.Close()

		// initialize a file writer
		writer := csv.NewWriter(file)

		// write the CSV headers
		headers := []string{
			"Url",
			"Image",
			"Name",
			"Price",
		}
		writer.Write(headers)

		// write each product as a CSV row
		for _, product := range products {
			// convert a Product to an array of strings
			record := []string{
				product.Url,
				product.Image,
				product.Name,
				product.Price,
			}

			// add a CSV record to the output file
			writer.Write(record)
		}
		defer writer.Flush()
	})

	// open the target URL
	c.Visit("a")

}
