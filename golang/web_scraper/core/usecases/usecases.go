package usecases

import (
	"fmt"
	"log"
	"scraper/core/domain"
	"scraper/core/ports"
)

type PropertyUseCase struct {
	scraperRepo  ports.ScraperRepository
	sqliteRepo   ports.SqliteRepository
	telegramRepo ports.TelegramRepository
}

func NewPropertyUseCase(
	scraperRepo ports.ScraperRepository,
	sqliteRepo ports.SqliteRepository,
	telegramRepo ports.TelegramRepository,
) ports.PropertyUseCase {
	return &PropertyUseCase{
		scraperRepo:  scraperRepo,
		sqliteRepo:   sqliteRepo,
		telegramRepo: telegramRepo,
	}
}

func (p *PropertyUseCase) UpdateData(chatID int64) error {
	properties_channel := make(chan domain.Property, 256)
	count := 0

	// scrap web
	err := p.scraperRepo.ScrapeWeb(properties_channel)
	if err != nil {
		log.Panic(err)
	}

	// update properties in db
	for property := range properties_channel {
		log.Printf("New property scraped!: %v", property.Name)
		count = count + 1
		// Get property
		currentProperty, err := p.sqliteRepo.GetById(property.Id)

		// If empty, insert
		if err != nil {
			log.Println("Property not found. Inserting...", property.Id)
			p.sqliteRepo.Insert(property)
			continue
		}

		// If unchanged, pass
		if property.Price == currentProperty.Price {
			log.Printf("Property %v unchanged\n", property.Id)
			continue
		}

		// If changed, update
		_, err = p.sqliteRepo.Update(property)
		if err != nil {
			log.Panic(err)
		}
	}

	err = p.telegramRepo.SendMessage(fmt.Sprintf("%v pisos nuevos encontrados", count), chatID)
	if err != nil {
		log.Println("Error sending telegram message")
	}

	return err
}

func (p *PropertyUseCase) GetProperties(chatID int64) (domain.Property, error) {
	// Get properties from db
	properties, err := p.sqliteRepo.GetAll()
	if err != nil {
		log.Panic(err)
	}
	// Create telegram messeges
	p.telegramRepo.SendProperties(properties, chatID)

	// Send tg messeges
	return domain.NewProperty(), nil
}
