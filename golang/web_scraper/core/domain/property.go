package domain

import "time"

// initialize a data structure to keep the scraped data
type Property struct {
	Id              string `json:"id"`
	Url             string `json:"url"`
	RealStateAgency string `json:"realStateAgency"`
	Image           string `json:"image"`
	Name            string `json:"name"`
	Price           string `json:"price"`
	Area            string `json:"area"`
	Timestamp       time.Time
}

func NewProperty() Property {
	return Property{}
}
