HORA_LABORAL_STATUS_OF_DATE=`curl --location --request POST 'https://gestion.horalaboral.com/v1/webapi/get_data_of_date' \
--header 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:90.0) Gecko/20100101 Firefox/90.0' \
--header 'Accept: */*' \
--header 'Accept-Language: es-ES,en;q=0.5' \
--header 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
--header 'Authorization: Basic NDYxNjA2OjAxOTU2OQ==' \
--header 'X-Requested-With: XMLHttpRequest' \
--header 'Origin: https://gestion.horalaboral.com' \
--header 'Connection: keep-alive' \
--header 'Referer: https://gestion.horalaboral.com/trabajador/' \
--header 'Cookie: _inout_session=eWU4mzG77io%2BaVmQiZxSwuE%2BJxIXZE%2FlS7sq27XK8L8%2BLHM7JDC4%2Fd7na5PWVy%2FsLMU8W1RMAUGHREn859g63M4JCZp5v1vk--japq%2F0zuDmDhd3c9--x3lR8u7pY%2B3y6VVhcuCwIg%3D%3D; _inout_session=dT5FG7kia3uixeoEBP4RcXrFQJFO%2Bpn8C8duSUe5m8ly8hFWgLpwqjwOTMhimQ1g05xfKYdDGgpct1I4ygSMxhBeOTS2km30--XrPEHLWt59lacxlU--yaQE4%2Fq%2BJ6%2Fn%2BMzFcQQ1wQ%3D%3D' \
--header 'Sec-Fetch-Dest: empty' \
--header 'Sec-Fetch-Mode: cors' \
--header 'Sec-Fetch-Site: same-origin' \
--header 'Pragma: no-cache' \
--header 'Cache-Control: no-cache' \
--data-raw 'date=2022-03-04&type=day'`

echo $HORA_LABORAL_STATUS_OF_DATE | jq

# * * * * 1-5 /Users/ggc/Workspace/github.com/code-samples/crontab/hora_laboral_sign.sh > $HOME/`date +\%Y\%m\%d\%H\%M\%S`-cron.log 2>&1