package main

import (
	"bufio"
	"container/list"
	"fmt"
	"os"
	"strconv"
	"strings"
)

/* TYPES */

// Player represents a player no matter the sport
type Player struct {
	Playername   string
	Nickname     string
	PlayerNumber string
}

// PlayerScore asf
type PlayerScore interface {
	getTotalPoints() int
}

// PlayerScoreBasket asf
type PlayerScoreBasket struct {
	PlayerNickname string
	Rebound        int
	Assist         int
	Guard          int
}

func (playerScoreBasket PlayerScoreBasket) getTotalPoints() int {
	// Do crazy calcs in here
	return playerScoreBasket.Rebound + playerScoreBasket.Assist + playerScoreBasket.Guard
}

// PlayerScoreHandball asf
type PlayerScoreHandball struct {
	PlayerNickname string
	GoalsMade      int
	GoalsReceived  int
}

func (playerScoreHandball PlayerScoreHandball) getTotalPoints() int {
	// Do crazy calcs in here
	return playerScoreHandball.GoalsMade + playerScoreHandball.GoalsReceived
}

// RankingEntry association between player and score
type RankingEntry struct {
	Nickname   string
	TotalScore int
}

/* FUNCTIONS */
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func checkCast(ok bool) {
	if !ok {
		panic("Some cast didn't went well")
	}
}

func printList(list *list.List) {
	fmt.Printf("Ranking: \n")
	for temp := list.Front(); temp != nil; temp = temp.Next() {
		fmt.Printf("| %v |\n", temp.Value)
	}
	fmt.Printf("\n\n")
}

func parsePlayer(sport string, attrs []string) Player {
	return Player{
		Playername:   attrs[0],
		Nickname:     attrs[1],
		PlayerNumber: attrs[2],
	}
}

func parseScore(sport string, nickname string, attrs []string) (PlayerScore, int) {
	var err error
	var score PlayerScore
	stats := make([]int, len(attrs))
	for i, attr := range attrs {
		stats[i], err = strconv.Atoi(attr)
		check(err)
	}

	switch sport {
	case "basket":
		score = PlayerScoreBasket{
			PlayerNickname: nickname,
			Rebound:        stats[0],
			Assist:         stats[1],
			Guard:          stats[2],
		}
		break
	case "handball":
		score = PlayerScoreHandball{
			PlayerNickname: nickname,
			GoalsMade:      stats[0],
			GoalsReceived:  stats[1],
		}
		break
	}
	total := score.getTotalPoints()
	return score, total
}

func bubbleUp(ranking *list.List, elem *list.Element) {
	rankingEntry, ok := elem.Value.(RankingEntry)
	checkCast(ok)

	for elem.Prev() != nil {
		prevRankingEntry, ok := elem.Prev().Value.(RankingEntry)
		checkCast(ok)

		if rankingEntry.TotalScore < prevRankingEntry.TotalScore {
			return
		}

		ranking.MoveBefore(elem, elem.Prev())
	}
}

func addPlayerScoreToRanking(
	ranking *list.List,
	player Player,
	score PlayerScore,
	matchTotal int,
) {
	for temp := ranking.Back(); temp != nil; temp = temp.Prev() {
		rankingEntry, ok := temp.Value.(RankingEntry)
		checkCast(ok)

		// New value. Insert at the first place before its higher
		if rankingEntry.TotalScore > matchTotal {
			ranking.InsertAfter(RankingEntry{
				Nickname:   player.Nickname,
				TotalScore: matchTotal,
			}, temp)
			return
		}

		// If exists, update and sort bubbling up
		if rankingEntry.Nickname == player.Nickname {
			rankingEntry.TotalScore += matchTotal
			temp.Value = rankingEntry
			bubbleUp(ranking, temp)
			return
		}
	}

	// This score is the highest one or empty. Push it
	ranking.PushFront(RankingEntry{
		Nickname:   player.Nickname,
		TotalScore: matchTotal,
	})
}

func getMVP(sports []string) {
	ranking := list.New()
	for _, sport := range sports {
		f, err := os.Open("./" + sport + ".txt")
		check(err)
		defer f.Close()

		scanner := bufio.NewScanner(f)
		scanner.Scan() // Remove title line

		whoWon(scanner)

		for scanner.Scan() {
			line := scanner.Text()
			attrs := strings.Split(line, ";")

			player := parsePlayer(sport, attrs[:5])
			score, matchTotal := parseScore(sport, player.Nickname, attrs[5:])
			addPlayerScoreToRanking(ranking, player, score, matchTotal)
		}
	}
	printList(ranking)
}

func main() {
	sports := []string{"basket", "handball"}
	getMVP(sports)
}
