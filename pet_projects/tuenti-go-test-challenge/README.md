## Issues
`fmt is not installed`

## Goal

Get MVP

MVP: Player with most points
Points calculation:
* Win = +10pts
* Basketball
|  Points table | Scored​ ​point | Rebound | Assist |
| ------------: | :------------: | :-----: | :----: |
|   Guard​ ​(G) |       2        |    3    |   1    |
| Forward​ ​(F) |       2        |    2    |   2    |
|  Center​ ​(C) |       2        |    1    |   3    |

* Handball
* 


## Requirements
* Test
* File directory
* Go patterns
* Types
* Error handling