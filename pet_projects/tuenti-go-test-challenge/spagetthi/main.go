package mainNope

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"container/list"
)

// FileSize is the Number of bytes to read
const FileSize = 1024

// Player represents a player no matter the sport
type Player struct {
	Playername   string
	Nickname     string
	PlayerNumber string
	Position     string
	SeasonPoints int
}

// Score represents the score of a player in a certain match
type Score struct {
	PlayerNickname string
	ScoredPoints int
	Rebounds     int
	Assists      int
}

// RankingEntry association between player and score
type RankingEntry struct {
	Nickname   string
	TotalScore int
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func calcPlayerStatistics(player Player) Player {
	multipliers := map[string]map[string]int{
		"G": {
			"Rebound": 2,
			"Assist":  3,
			"Guard":   1,
		},
		"F": {
			"Rebound": 2,
			"Assist":  2,
			"Guard":   2,
		},
		"C": {
			"Rebound": 2,
			"Assist":  1,
			"Guard":   3,
		},
	}

	for pointsMult := range multipliers[player.Position] {
		player.SeasonPoints += multipliers[player.Position][pointsMult] * player.Scores[pointsMult]
	}

	return player
}

func parseLine(attrs []string) Player {
	scoredPoints, err := strconv.Atoi(attrs[5])
	check(err)
	rebounds, err := strconv.Atoi(attrs[6])
	check(err)
	assists, err := strconv.Atoi(attrs[7])
	check(err)

	// playerScore := Score{
	// 	ScoredPoints: scoredPoints,
	// 	Rebounds:     rebounds,
	// 	Assists:      assists,
	// }
	player := Player{
		Playername:   attrs[0],
		Nickname:     attrs[1],
		PlayerNumber: attrs[2],
		Position:     attrs[3],
		SeasonPoints: 0,
		Scores:       nil,
	}
	player.Scores = map[string]int
	return player
}
func checkFileEntry(attrs []string) bool {
	// 3. Parse file - line
	if len(attrs) == 0 {
		println("This match has no values")
		return false
	} else if len(attrs) < 8 {
		println("This match is incomplete")
		return false
	}
	return true
}

func getMVPofSport() {

	f, err := os.Open("./basket.txt")
	check(err)
	defer f.Close()
	scanner := bufio.NewScanner(f)
	scanner.Scan() // Remove first line
	ranking := list.New()
	for scanner.Scan() {
		fmt.Printf("\n[LOG] getting new line \n")
		line := scanner.Text()
		attrs := strings.Split(line, ";")

		if !checkFileEntry(attrs) {
			continue
		}

		// 3.1 Basketball format
		player := parseLine(attrs)

		fmt.Printf("player %v, %v\n", player, player.Score)
		// 2.1 calculate player's score
		player = calcPlayerStatistics(player)

		// 3.3 Create ranking entry for user
		// 4. Find player in ranking
		isNewPlayer := true
		var firstHigherRank *list.Element

		for temp := ranking.Front(); temp != nil; temp = temp.Next() {
			rankingEntry, ok := temp.Value.(RankingEntry)
			fmt.Printf("ranking entry %v, ok? %v\n", rankingEntry, ok)

			if rankingEntry.Nickname == player.Nickname {
				isNewPlayer = false
				rankingEntry.TotalScore = player.Score.ScoredPoints
				temp.Value = rankingEntry
				break
			}

			if rankingEntry.TotalScore > player.Score.ScoredPoints {
				firstHigherRank = temp
			}
		}

		if ranking.Front() == nil || firstHigherRank == nil {
			ranking.PushFront(RankingEntry{
				Nickname:   player.Nickname,
				TotalScore: player.Score.ScoredPoints,
			})
			continue
		}
		fmt.Printf("[LOG] firstHigherMark %v\n", firstHigherRank.Value)
		if isNewPlayer {
			ranking.InsertAfter(RankingEntry{
				Nickname:   player.Nickname,
				TotalScore: player.Score.ScoredPoints,
			}, firstHigherRank)
		}
	}
	for temp := ranking.Front(); temp != nil; temp = temp.Next() {
		fmt.Println(temp.Value)
	}
}

func main2() {

	/* Pseudo */
	// 1. For all files (1 file - 1 match)
	// 2. Read file
	// 3. Parse file (1 entry - 1 player - 1 score)
	// 3.1 Basketball format:
	//     player​ ​name;nickname;number;team​ ​name;position;scored​ ​points;rebounds;assists
	// 3.2 Handball format:
	//     player​ ​name;nickname;number;team​ ​name;position;goals​ ​made;goals​ ​received
	// 4. Store player score in memory in a "Cola de prioridad"

	// 2. Read file

	// b1 := make([]byte, FileSize)
	// n1, err := f.Read(b1)
	// check(err)
	// fmt.Printf("%d bytes: %s\n", n1, string(b1))
}
