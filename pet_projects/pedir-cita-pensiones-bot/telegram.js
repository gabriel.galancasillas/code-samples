const { Telegraf, Telegram } = require('telegraf')
const events = require('events');
const eventEmitter = new events.EventEmitter();

const bot = new Telegraf(process.env.BOT_TOKEN)
bot.start((ctx) => {
    ctx.reply('Welcome')
})
bot.on('message', (ctx) => {
    eventEmitter.emit('scream', ctx.message.text)
})
bot.launch()

function send(message) {
    const telegram = new Telegram(process.env.BOT_TOKEN)
    telegram.sendMessage('1979206', message);
}

function readAnswer() {
    return new Promise(resolve => {
        eventEmitter.on('scream', msg => {
            resolve(msg);
        });
    })
}

module.exports = {
    send,
    readAnswer
}