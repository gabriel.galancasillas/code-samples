const bot = require('./telegram');

async function scraper(browser) {
    const url = 'https://w6.seg-social.es/ProsaInternetAnonimo/OnlineAccess?ARQ.SPM.ACTION=LOGIN&ARQ.SPM.APPTYPE=SERVICE&ARQ.IDAPP=XV106001&ORGANISMO=I';
    let page = await browser.newPage();
    await page.goto(url);

    // Wait for the required DOM to be rendered
    await page.waitForSelector('#ARQpie');

    // Get the link to all the required books
    await page.$eval('#nombre', nameInput => nameInput.value = 'Pilar Casillas Gonzalez');
    await page.$eval('#tipo', nameInput => nameInput.value = '1');
    await page.$eval('#ipfnumero', nameInput => nameInput.value = '05215592C');
    await page.$eval('#telefono', nameInput => nameInput.value = '660133047');
    await page.$eval('#email', nameInput => nameInput.value = 'pilar.casillas@hotmail.com');
    await page.$eval('#radioDiaHoraProvincia', nameInput => nameInput.click());
    await page.$eval('#provincia1', nameInput => {
        nameInput.disabled = false;
        nameInput.value = '28';
    });

    // Get captcha
    const captchaQuestion = await page.$eval('.p2', nameInput => nameInput.textContent);
    const captchaChoices = await page.$eval('.p0', nameInput => nameInput.textContent);
    console.log('[LOG] captcha: ', captchaQuestion, captchaChoices);
    bot.send(`${captchaQuestion} -> ${captchaChoices}`)

    // Read bot response
    const answer = await bot.readAnswer();
    console.log('[LOG] answer: ', answer);
    
    // Write response into captchaInput
    await page.$eval('#ARQ\\.CAPTCHA', (nameInput, value) => nameInput.value = value, answer);
    
    // Click "Siguiente"
    await page.$eval('#SPM\\.ACC\\.SIGUIENTE', nameInput => nameInput.click());
    await page.waitForNavigation({ waitUntil: 'networkidle2' });

    // Select the first option (Solicitud de juvilacion)
    await page.$eval('input[name="servicioSeleccionado"]', el => el.click());

    // Click "Siguiente"
    await page.$eval('#SPM\\.ACC\\.CONTINUAR_TRAS_SELECCIONAR_SERVICIO', nameInput => nameInput.click());
    await page.waitForNavigation({ waitUntil: 'networkidle2' });

}

module.exports = {
    scraper
};