module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false,
  theme: {
    extend: {
      colors: {
        purple: '#ff0000',
      },
    },
  },
  variants: {},
  plugins: [],
};
