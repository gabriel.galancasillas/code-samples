use std::collections::HashMap;

use crate::{
    v1::constants::{ProbeType, PROBE_SET},
    v1::graph::{Graph, Probe, Vertex},
};

use rayon::prelude::*;

/// State of the problem
/// graph: Inmutable
/// probes_list: Dependent to current instance
/// solution_pairs: Dependent to current instance
/// scores: Dependent to current instance and state
#[derive(Clone, Debug)]
pub struct State {
    pub graph: Graph,            // Inmutable
    pub probes_list: Vec<Probe>, // Dependent to current instance
    /// (vertex id, probe id)
    pub solution_pairs: HashMap<u16, u16>,
    // scores: HashMap<u16, f32>,         // Dependent to current instance and state
}
impl State {
    pub fn build_final_graph(&self) -> Graph {
        let mut new_graph = self.graph.clone();

        new_graph.vertices.iter_mut().for_each(|v: &mut Vertex| {
            let probe = self.solution_pairs.iter().find(|s| v.id == *s.0);
            if probe.is_none() {
                return;
            }
            let probe_id = probe.unwrap().1;
            v.probe = PROBE_SET
                .iter()
                .find(|p| p.id == *probe_id)
                .unwrap_or_else(|| panic!("Fuck. Not again..."))
                .clone();
        });

        new_graph
    }
}

/// Algorithm to get maximum revenue given a graph and a list of probes
/// 0: State
/// 1: highest production
pub fn find_highest_production(mut state: State) -> (State, f32) {
    let mut best_solution: (State, f32) = (state.clone(), -1.0);

    if state.solution_pairs.len() >= 6 {
        // println!(
            // "[LOG] latest tries: {:#?} {:?}",
        //     state.probes_list, state.solution_pairs
        // );
    }

    if is_first_iteration(&state) {
        state = init_state(state);
    }

    // println!("[LOG] Iteration {:#?}", state.solution_pairs.len() + 1);
    if is_base_case(&state) {
        // println!(
            // "[LOG] {} Base case reached",
        //     Local::now().format("%Y-%m-%d %H:%M:%S%.3f")
        // );
        let production = calculate_production(&state);
        return (state, production);
    }

    state.probes_list.par_iter().enumerate().for_each(|(i, p)| {
        // println!("[LOG] Testing probe {:#?}", p);
        let scores = calculate_scores(&state, p.probe_type);
        // println!("[LOG] Scores {:#?}", scores);
        let candidates = get_candidates(p, &state, scores);
        // println!("[LOG] Found {:#?} candidates", candidates);

        candidates.iter().for_each(|v| {
            // println!("[LOG] Testing probe {:#?} in candidate {:#?}", p.id, v);
            let current_state = set_probe_into_vertex(i, v, &state);

            let current_solution = find_highest_production(current_state);

            if is_better(current_solution.1, best_solution.1) {
                best_solution.0 = current_solution.0;
                best_solution.1 = current_solution.1;
            }
        });
    });

    (best_solution.0, best_solution.1)
}

// @param state: Current state
// @return: True if is the first iteration, false otherwise
pub fn is_first_iteration(state: &State) -> bool {
    state.solution_pairs.is_empty()
}

// @param state: Current state
// @return: State with the probes list sorted
pub fn init_state(state: State) -> State {
    // if state.probes_list.len() < 5 {
    //     return state;
    // }

    let sorted_probes_list = sort_probe_list(&state.probes_list);

    let mut purged_probes_list = vec![];

    // Take every duplicator probe
    sorted_probes_list.iter().enumerate().for_each(|(i, p)| {
        if p.probe_type == ProbeType::DUPLICATOR {
            purged_probes_list.push(sorted_probes_list.get(i).unwrap().to_owned());
        }
    });
    // Take every boost probe
    sorted_probes_list.iter().enumerate().for_each(|(i, p)| {
        if p.probe_type == ProbeType::BOOST {
            purged_probes_list.push(sorted_probes_list.get(i).unwrap().to_owned());
        }
    });
    // Fill
    sorted_probes_list.iter().enumerate().for_each(|(i, p)| {
        if purged_probes_list.len() < state.graph.vertices.len()
            && p.probe_type != ProbeType::BOOST
            && p.probe_type != ProbeType::DUPLICATOR
        {
            purged_probes_list.push(sorted_probes_list.get(i).unwrap().to_owned());
        }
    });

    let mut new_state = state;

    let len = if new_state.graph.vertices.len() > sorted_probes_list.len() {
        sorted_probes_list.len()
    } else {
        new_state.graph.vertices.len()
    };
    new_state.probes_list = sorted_probes_list[0..len]
        .iter()
        .filter(|x| x.probe_type != ProbeType::REVENUE)
        .cloned()
        .collect();

    new_state
}

// @param state: Current state
// @return: True if is the base case, false otherwise
pub fn is_base_case(state: &State) -> bool {
    state.probes_list.is_empty() || state.graph.vertices.len() <= state.solution_pairs.len()
}

// @param state: Current state
// @return: Total production
pub fn calculate_production(state: &State) -> f32 {
    let final_graph = state.build_final_graph();
    final_graph.total_production()
}

// @param state: Current state
// @return: According to the current state and a given probe, calculate the score of each vertex where for:
// ProbeType::DUPLICATOR is number of edges
// ProbeType::BOOST adyacent to DUPLICATOR probes and with more edges
// ProbeType::MINING is production * boost of adyacent probes * DUPLICATOR probes adyacent
// ProbeType::REVENUE is revenue * boost of adyacent probes * DUPLICATOR probes adyacent
pub fn calculate_scores(state: &State, probe: ProbeType) -> HashMap<u16, f32> {
    let mut scores = HashMap::new();

    state.graph.vertices.iter().for_each(|v| {
        scores.insert(v.id, 0.0);
        if vertex_has_probe(v.id, &state.solution_pairs) {
            return;
        }
        let score;
        match probe {
            // The more edges, the better
            ProbeType::DUPLICATOR => {
                score = state
                    .graph
                    .edges
                    .iter()
                    .filter(|e| e.from == v.id || e.to == v.id)
                    .count() as f32;
                scores.insert(v.id, score);
            }
            // If close to DUPLICATOR, the better.
            // Secondary, the more edges, the better
            // The lower the production, the better
            ProbeType::BOOST => {
                let duplicator_vertex_id_and_probe_id = state
                    .solution_pairs
                    .iter()
                    .find(|s| *s.1 == DG1.id)
                    .unwrap_or((&0, &0));
                let has_duplicator_close = state
                    .graph
                    .edges
                    .iter()
                    .filter(|e| {
                        (e.from == v.id && e.to == *duplicator_vertex_id_and_probe_id.0)
                            || (e.to == v.id && e.from == *duplicator_vertex_id_and_probe_id.0)
                    })
                    .count()
                    > 0;

                let score_from_edges_count = state
                    .graph
                    .edges
                    .iter()
                    .filter(|e| e.from == v.id || e.to == v.id)
                    .count() as f32
                    / 10.0;
                let score_from_production = (1000.0 - find_production_rate(v.production)) / 10000.0;
                if has_duplicator_close {
                    score = score_from_edges_count + 1.0 + score_from_production;
                } else {
                    score = score_from_edges_count + score_from_production;
                }
                scores.insert(v.id, score);
            }
            // If close to DUPLICATOR or BOOSTER, the better
            // Secondary, the higher the production, the better
            ProbeType::MINING => {
                let vertices_with_boosters_or_duplicators: Vec<u16> = state
                    .solution_pairs
                    .iter()
                    .filter(|s| [BG1, BG2, DG1].iter().any(|x| x.id == *s.1))
                    .map(|s| *s.0)
                    .collect();

fn is_base_case(n_probes_left: usize, n_vertices: usize, n_solutions: usize) -> bool {
    return n_probes_left == 0 || n_vertices == n_solutions;
}

/// Fill graph with scores acoring to current probe
// fn calculate_scoring(probe: &Probe, graph: &mut Graph) {
//     graph.vertices.iter_mut().for_each(|v| match probe.probe_type {
//         ProbeType::DUPLICATOR => {
//              let edges = graph.edges.iter().filter(|e| e.from == v.id || e.to == v.id).fold(0.0, |acc, _| acc + 1.0);
//              v.score = edges;
//         }
//         ProbeType::BOOST => println!("[LOG] BOOST"),
//         ProbeType::MINING => println!("[LOG] MINING"),
//         _ => println!("[LOG] BANANA"),
//     })
// }

/// Solution purger
/// DUPLICATOR: In vertices with more than 1 edge
/// BOOSTER: In vertices with more than 1 edge and adyacent to highest production vertices or fill
/// MINING: Around BOOSTERS and DUPLICATORS or fill
fn is_candidate_vertex(
    p: &Probe,
    vertex_id: u16,
    _graph: &Graph,
    solution: &Vec<SolutionPair>,
) -> bool {
    println!("[LOG] is_candidate {:?}", solution.iter().any(|s| s.vertex_id == vertex_id));
    if solution.iter().any(|s| s.vertex_id == vertex_id) {
        return false;
    }

    if probe.probe_type == ProbeType::REVENUE {
        return vec![];
    }

    let max_scores_vertices: Vec<u16> = scores
        .iter()
        .filter(|x| x.1 == max_score)
        .map(|x| *x.0)
        .collect();

    let a: Vec<u16> = state
        .graph
        .vertices
        .iter()
        .filter(|x| max_scores_vertices.contains(&x.id))
        .map(|x| x.id)
        .collect();

    // if a.is_empty() {
    //     vec![state.graph.vertices.iter()
    //         .find(|v| state.solution_pairs.contains_key(&v.id)).unwrap().id]
    // } else {
    a
    // }
}

// @param probe_index: Current probe index
// @param vertex_id: Current vertex id
// @param state: Current state
// @return: New state with the probe set into the vertex
fn set_probe_into_vertex(probe_index: usize, vertex_id: &u16, state: &State) -> State {
    let mut new_state = state.clone();
    new_state
        .solution_pairs
        .insert(*vertex_id, new_state.probes_list[probe_index].id);
    new_state.probes_list.remove(probe_index);
    new_state
}

// @param current_solution: Current solution
// @param best_solution: Best solution
// @return: True if current solution is better than best solution, false otherwise
fn is_better(current_solution: f32, best_solution: f32) -> bool {
    current_solution > best_solution
}

fn sort_probe_list(probes_list: &[Probe]) -> Vec<Probe> {
    let mut sorted_probe_list = probes_list.to_vec();
    sorted_probe_list.sort();
    sorted_probe_list
}

#[cfg(test)]
mod tests {

    use std::vec;

    use super::*;
    use crate::{
        v1::constants::{ProbeGrade, B, BG1, BG2, DG1, MG1, MG10, RG1},
        v1::graph::{Edge, Vertex},
        v1::test_utils,
    };

    #[test]
    fn calculate_scores_test() {
        let mut v: Vec<Vertex> = test_utils::create_vertices(&vec![B.id, B.id, B.id, B.id, B.id]);
        v[0].production = 'C';
        v[1].production = 'A';
        v[2].production = 'B';
        v[3].production = 'C';
        v[4].production = 'A';
        let e = test_utils::create_edges(&[(0, 1), (1, 2), (1, 3), (2, 3), (1, 4)], &v);
        let graph = Graph::new(v, e);
        let solution_pairs = HashMap::new();
        // solution_pairs.insert(graph.vertices[1].id, DG1.id);
        let mut state = State {
            graph,
            probes_list: vec![MG1, BG1, BG2, DG1],
            solution_pairs,
        };

        let expected_list = vec![
            (DG1, vec![1.0, 4.0, 2.0, 2.0, 1.0]),
            (BG1, vec![1.1750001, 0.0, 1.2650001, 1.2750001, 1.15]),
            (MG1, vec![1.25, 0.0, 2.35, 0.0, 1.5]),
        ];

        expected_list.iter().enumerate().for_each(|(i, expected)| {
            let actual = calculate_scores(&state, expected.0.probe_type);

            for j in 0..expected.1.len() {
                assert_eq!(
                    expected.1[j],
                    *actual.get(&(100 + j as u16)).unwrap(),
                    "i: {}, j: {}",
                    i,
                    j
                );
            }

            let best_candidate = actual
                .iter()
                .max_by(|a, b| a.1.partial_cmp(b.1).unwrap())
                .unwrap();

            state
                .solution_pairs
                .insert(*best_candidate.0, expected.0.id);
        });
    }

    #[test]
    fn find_highest_revenue_1_test() {
        let id1 = 101;
        let id2 = 102;
        let id3 = 103;

        let v1 = Vertex {
            id: id1,
            production: 'C',
            revenue: 'A',
            probe: B,
        };
        let v2 = Vertex {
            id: id2,
            production: 'A',
            revenue: 'B',
            probe: B,
        };
        let v3 = Vertex {
            id: id3,
            production: 'B',
            revenue: 'B',
            probe: B,
        };

        let e1 = Edge { from: id1, to: id2 };
        let e2 = Edge { from: id2, to: id3 };
        let graph = Graph {
            vertices: vec![v1, v2, v3],
            edges: vec![e1, e2],
        };
        let probes_list: Vec<Probe> = vec![MG10, BG1, BG2];
        let solution_pairs = HashMap::new();
        let state = State {
            graph,
            probes_list,
            solution_pairs,
        };

        let actual = find_highest_production(state);

        // println!("[LOG] actual: {:#?}", actual);

        assert_eq!(
            250.0 * 0.3 + 500.0 * 3.0 * 1.5 * 2.0 + 350.0 * 0.3,
            actual.1
        );
    }

    #[test]
    fn find_highest_revenue_bg1_bg1_test() {
        let probes_list: Vec<Probe> = vec![BG1, BG2];
        let solution_pairs = HashMap::new();

        let mut v: Vec<Vertex> = test_utils::create_vertices(&vec![B.id, B.id]);
        v[1].production = 'C';
        let e = test_utils::create_edges(&[(0, 1)], &v);
        let graph = Graph::new(v, e);
        let state = State {
            graph,
            probes_list,
            solution_pairs,
        };

        let actual = find_highest_production(state);

        assert_eq!((500.0 * 0.3 * 2.0) + (250.0 * 0.3 * 1.5), actual.1);
        assert_eq!(BG1.id, *actual.0.solution_pairs.get(&100).unwrap());
        assert_eq!(BG2.id, *actual.0.solution_pairs.get(&101).unwrap());
    }

    #[test]
    #[ignore = "Every solution should have at least 1 production probe"]
    fn find_highest_revenue_more_probes_test() {
        let probes_list: Vec<Probe> = vec![BG1, BG2, MG10, RG1];
        let solution_pairs = HashMap::new();

        let mut v: Vec<Vertex> = test_utils::create_vertices(&vec![B.id, B.id]);
        v[1].production = 'C';
        let e = test_utils::create_edges(&[(0, 1)], &v);
        let graph = Graph::new(v, e);
        let state = State {
            graph,
            probes_list,
            solution_pairs,
        };

        let actual = find_highest_production(state);

        assert_eq!((500.0 * 3.0 * 2.0) + (250.0 * 0.3), actual.1);
        assert_eq!(MG10.id, *actual.0.solution_pairs.get(&100).unwrap());
        assert_eq!(BG2.id, *actual.0.solution_pairs.get(&101).unwrap());
    }

    #[test]
    fn find_highest_revenue_of_bg1_dg1_mg10_and_bg2_test() {
        let probes_list: Vec<Probe> = vec![MG10, BG1, BG2, DG1];
        let solution_pairs = HashMap::new();

        let mut v: Vec<Vertex> = test_utils::create_vertices(&vec![B.id, B.id, B.id, B.id]);
        v[0].production = 'C';
        v[1].production = 'A';
        v[2].production = 'B';
        v[3].production = 'C';
        let e = test_utils::create_edges(&[(0, 1), (1, 2), (2, 3)], &v);
        let graph = Graph::new(v, e);
        let state = State {
            graph,
            probes_list,
            solution_pairs,
        };

        let actual = find_highest_production(state);

        assert_eq!(
            (250.0 * 0.3) + (500.0 * 3.0 * 1.5 * 2.0) + (350.0 * 3.3 * 2.0) + (250.0 * 0.3 * 2.0),
            actual.1
        );
        assert_eq!(BG1.id, *actual.0.solution_pairs.get(&100).unwrap());
        assert_eq!(MG10.id, *actual.0.solution_pairs.get(&101).unwrap());
        assert_eq!(DG1.id, *actual.0.solution_pairs.get(&102).unwrap());
        assert_eq!(BG2.id, *actual.0.solution_pairs.get(&103).unwrap());
    }

    #[test]
    fn find_highest_revenue_of_many_unordered_probes_test() {
        let probes_list: Vec<Probe> = vec![
            MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1,
            MG10, BG1, BG2, DG1,
        ];
        let solution_pairs = HashMap::new();

        let mut v: Vec<Vertex> = test_utils::create_vertices(&vec![B.id, B.id, B.id, B.id]);
        v[0].production = 'C';
        v[1].production = 'A';
        v[2].production = 'B';
        v[3].production = 'C';
        let e = test_utils::create_edges(&[(0, 1), (1, 2), (2, 3)], &v);
        let graph = Graph::new(v, e);
        let state = State {
            graph,
            probes_list,
            solution_pairs,
        };

        let actual = find_highest_production(state);

        assert_eq!(
            (250.0 * 0.3) + (500.0 * 3.0 * 1.5 * 2.0) + (350.0 * 3.3 * 2.0) + (250.0 * 0.3 * 2.0),
            actual.1
        );
        assert_eq!(BG1.id, *actual.0.solution_pairs.get(&100).unwrap());
        assert_eq!(MG10.id, *actual.0.solution_pairs.get(&101).unwrap());
        assert_eq!(DG1.id, *actual.0.solution_pairs.get(&102).unwrap());
        assert_eq!(BG2.id, *actual.0.solution_pairs.get(&103).unwrap());
    }

    #[test]
    fn init_state_many_many_test() {
        let probes_list: Vec<Probe> = vec![
            MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1,
            MG10, BG1, BG2, DG1,
        ];

        let mut v: Vec<Vertex> = test_utils::create_vertices(&vec![B.id, B.id, B.id, B.id]);
        v[0].production = 'C';
        v[1].production = 'A';
        v[2].production = 'B';
        v[3].production = 'C';
        let e = test_utils::create_edges(&[(0, 1), (1, 2), (2, 3)], &v);
        let graph = Graph::new(v, e);
        let state = State {
            graph,
            probes_list,
            solution_pairs: HashMap::new(),
        };

        let expected = init_state(state);

        assert_eq!(4, expected.probes_list.len());
        assert_eq!(ProbeType::DUPLICATOR, expected.probes_list[0].probe_type);
        assert_eq!(ProbeGrade::I, expected.probes_list[0].probe_grade);
        assert_eq!(ProbeType::BOOST, expected.probes_list[1].probe_type);
        assert_eq!(ProbeGrade::II, expected.probes_list[1].probe_grade);
        assert_eq!(ProbeType::BOOST, expected.probes_list[2].probe_type);
        assert_eq!(ProbeGrade::I, expected.probes_list[2].probe_grade);
        assert_eq!(ProbeType::MINING, expected.probes_list[3].probe_type);
        assert_eq!(ProbeGrade::X, expected.probes_list[3].probe_grade);
    }

    // FIXME: Not even defined;
    // #[test]
    // fn get_candidates_test() {
    //     let probes_list: Vec<Probe> = vec![
    //         DG1, BG2, BG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1,
    //         MG1, MG10, MG9,
    //     ];
    //     let solution_pairs = HashMap::new();

    //     let mut v: Vec<Vertex> = test_utils::create_vertices(&vec![B.id, B.id, B.id, B.id]);
    //     v[0].production = 'C';
    //     v[1].production = 'B';
    //     v[2].production = 'A';
    //     v[3].production = 'C';
    //     let e = test_utils::create_edges(&vec![(0, 1), (1, 2), (1, 3)], &v);
    //     let graph = Graph::new(v, e);
    //     let state = State {
    //         graph,
    //         probes_list,
    //         solution_pairs,
    //         scores: HashMap::new(),
    //     };

    //     let mut actual = get_candidates(&state.probes_list[0], &state);

    //     assert_eq!(1, actual.len());
    //     assert_eq!(1, actual[0]);

    //     actual = get_candidates(&state.probes_list[23], &state);

    //     assert_eq!(1, actual.len());
    //     assert_eq!(2, actual[0]);
    // }

    #[test]
    fn sort_probe_list_test() {
        let probes_list: Vec<Probe> = vec![
            MG1, MG1, MG1, MG1, MG1, BG2, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG10,
            MG9, BG1, DG1,
        ];
        let expected: Vec<Probe> = vec![
            DG1, BG2, BG1, MG10, MG9, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1, MG1,
            MG1, MG1, MG1,
        ];

        let actual = sort_probe_list(&probes_list);

        for i in 0..(expected.len() - 1) {
            assert_eq!(expected[i].id, actual[i].id);
        }
    }
}
