use crate::{
    v1::constants::{B, PROBE_SET},
    v1::graph::{Edge, Vertex},
};

/// args are probe ids
pub fn create_vertices(ids: &Vec<u16>) -> Vec<Vertex> {
    let mut vertices = vec![];
    let mut vertex_id = 100;
    for id in ids {
        let probe = PROBE_SET.iter().find(|p| p.id.eq(id)).unwrap_or(&B);
        let mut v = Vertex::new(vertex_id, 'A', 'A');
        v.probe = probe.clone();
        vertices.push(v);
        vertex_id += 1;
    }
    // Test 1
    // let id = 100;
    // let mut v1: Vertex = Vertex::new(id, 'A', 'A');
    // v1.probe = B;

    // Test 2
    // let id1 = 103;
    // let id2 = 104;
    // let mut v1: Vertex = Vertex::new(id1, 'B', 'F');
    // v1.probe = MG7;
    // let mut v2: Vertex = Vertex::new(id2, 'C', 'E');
    // v2.probe = BG1;

    // Test 3
    // let id1 = 103;
    // let id2 = 104;
    // let mut v1: Vertex = Vertex::new(id1, 'B', 'F');
    // v1.probe = MG7;
    // let mut v2: Vertex = Vertex::new(id2, 'C', 'E');
    // v2.probe = BG1;

    // Test 4
    //     let id1 = 105;
    //     let id2 = 106;
    //     let id3 = 107;
    //     let mut v1: Vertex = Vertex::new(id1, 'C', 'F');
    //     v1.probe = MG10;
    //     let mut v2: Vertex = Vertex::new(id2, 'A', 'A');
    //     v2.probe = BG1;
    //     let mut v3: Vertex = Vertex::new(id3, 'A', 'A');
    //     v3.probe = BG2;

    // Test 5
    //     let id1 = 108;
    //     let id2 = 109;
    //     let id3 = 110;
    //     let mut v1: Vertex = Vertex::new(id1, 'A', 'A');
    //     v1.probe = MG1;
    //     let mut v2: Vertex = Vertex::new(id2, 'B', 'B');
    //     v2.probe = DG1;
    //     let mut v3: Vertex = Vertex::new(id3, 'C', 'C');
    //     v3.probe = BG2;

    // Test 6
    //     let id1 = 111;
    //     let id2 = 112;
    //     let id3 = 113;
    //     let id4 = 114;
    //     let mut v1: Vertex = Vertex::new(id1, 'A', 'A');
    //     v1.probe = BG2;
    //     let mut v2: Vertex = Vertex::new(id2, 'B', 'B');
    //     v2.probe = DG1;
    //     let mut v3: Vertex = Vertex::new(id3, 'A', 'A');
    //     v3.probe = MG1;
    //     let mut v4: Vertex = Vertex::new(id4, 'C', 'C');
    //     v4.probe = BG1;

    // Test 7
    //     let id1 = 115;
    //     let id2 = 116;
    //     let mut v1: Vertex = Vertex::new(id1, 'A', 'A');
    //     v1.probe = BG2;
    //     let mut v2: Vertex = Vertex::new(id2, 'C', 'C');
    //     v2.probe = BG1;

    // Test 8
    //     let id1 = 117;
    //     let id2 = 118;
    //     let mut v1: Vertex = Vertex::new(id1, 'A', 'A');
    //     v1.probe = DG1;
    //     let mut v2: Vertex = Vertex::new(id2, 'C', 'C');
    //     v2.probe = BG2;

    vertices
}

pub fn create_edges(pairs: &[(u16, u16)], vertices: &[Vertex]) -> Vec<Edge> {
    let mut edges = vec![];
    pairs.iter().for_each(|p| {
        edges.push(
            Edge {
                from: vertices[usize::try_from(p.0).unwrap()].id,
                to: vertices[usize::try_from(p.1).unwrap()].id,
            },
        )
    });
    edges
    // let e1: Edge = Edge {
    //     from: vertices[0].id,
    //     to: vertices[0].id,
    // };
}
