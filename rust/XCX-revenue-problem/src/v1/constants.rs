// use crate::graph::new_probe;
// use crate::graph::Probe;

use crate::v1::graph::Probe;

pub static RATES: [(&str, char, f32); 9] = [
    ("M", 'A', 500.0),
    ("M", 'B', 350.0),
    ("M", 'C', 250.0),
    ("R", 'A', 750.0),
    ("R", 'B', 650.0),
    ("R", 'C', 550.0),
    ("R", 'D', 450.0),
    ("R", 'E', 300.0),
    ("R", 'F', 200.0),
];
pub fn find_production_rate(production: char) -> f32 {
    return RATES
        .iter()
        .find(|rate| rate.0.eq("M") && rate.1 == production)
        .unwrap()
        .2;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ProbeType {
    BASIC,
    MINING,
    REVENUE,
    BOOST,
    DUPLICATOR,
}
pub fn parse_probe_type(probe_type: &str) -> ProbeType {
    match probe_type {
        "M" => ProbeType::MINING,
        "R" => ProbeType::REVENUE,
        "B" => ProbeType::BOOST,
        "D" => ProbeType::DUPLICATOR,
        _ => ProbeType::BASIC,
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ProbeGrade {
    I,
    II,
    III,
    IV,
    V,
    VI,
    VII,
    VIII,
    IX,
    X,
}
pub fn parse_probe_grade(probe_grade: &str) -> ProbeGrade {
    match probe_grade {
        "1" => ProbeGrade::I,
        "2" => ProbeGrade::II,
        "3" => ProbeGrade::III,
        "4" => ProbeGrade::IV,
        "5" => ProbeGrade::V,
        "6" => ProbeGrade::VI,
        "7" => ProbeGrade::VII,
        "8" => ProbeGrade::VIII,
        "9" => ProbeGrade::IX,
        "10" => ProbeGrade::X,
        _ => panic!("Probe grade not recognized"),
    }
}

pub const B: Probe = new_probe(0, ProbeType::BASIC, ProbeGrade::I, 0.5, 0.5, 0.0);
pub const MG1: Probe = new_probe(1, ProbeType::MINING, ProbeGrade::I, 1.0, 0.3, 0.0);
pub const MG2: Probe = new_probe(2, ProbeType::MINING, ProbeGrade::II, 1.2, 0.3, 0.0);
pub const MG3: Probe = new_probe(3, ProbeType::MINING, ProbeGrade::III, 1.4, 0.3, 0.0);
pub const MG4: Probe = new_probe(4, ProbeType::MINING, ProbeGrade::IV, 1.6, 0.3, 0.0);
pub const MG5: Probe = new_probe(5, ProbeType::MINING, ProbeGrade::V, 1.8, 0.3, 0.0);
pub const MG6: Probe = new_probe(6, ProbeType::MINING, ProbeGrade::VI, 2.0, 0.3, 0.0);
pub const MG7: Probe = new_probe(7, ProbeType::MINING, ProbeGrade::VII, 2.2, 0.3, 0.0);
pub const MG8: Probe = new_probe(8, ProbeType::MINING, ProbeGrade::VIII, 2.4, 0.3, 0.0);
pub const MG9: Probe = new_probe(9, ProbeType::MINING, ProbeGrade::IX, 2.7, 0.3, 0.0);
pub const MG10: Probe = new_probe(10, ProbeType::MINING, ProbeGrade::X, 3.0, 0.3, 0.0);
pub const RG1: Probe = new_probe(11, ProbeType::REVENUE, ProbeGrade::I, 0.3, 1.0, 0.0);
pub const RG2: Probe = new_probe(12, ProbeType::REVENUE, ProbeGrade::II, 0.3, 1.2, 0.0);
pub const RG3: Probe = new_probe(13, ProbeType::REVENUE, ProbeGrade::III, 0.3, 1.4, 0.0);
pub const RG4: Probe = new_probe(14, ProbeType::REVENUE, ProbeGrade::IV, 0.3, 1.6, 0.0);
pub const RG5: Probe = new_probe(15, ProbeType::REVENUE, ProbeGrade::V, 0.3, 1.8, 0.0);
pub const RG6: Probe = new_probe(16, ProbeType::REVENUE, ProbeGrade::VI, 0.3, 2.0, 0.0);
pub const RG7: Probe = new_probe(17, ProbeType::REVENUE, ProbeGrade::VII, 0.3, 2.2, 0.0);
pub const RG8: Probe = new_probe(18, ProbeType::REVENUE, ProbeGrade::VIII, 0.3, 2.4, 0.0);
pub const RG9: Probe = new_probe(19, ProbeType::REVENUE, ProbeGrade::IX, 0.3, 2.7, 0.0);
pub const RG10: Probe = new_probe(20, ProbeType::REVENUE, ProbeGrade::X, 0.3, 3.0, 0.0);
pub const BG1: Probe = new_probe(21, ProbeType::BOOST, ProbeGrade::I, 0.3, 0.3, 1.5);
pub const BG2: Probe = new_probe(22, ProbeType::BOOST, ProbeGrade::II, 0.3, 0.3, 2.0);
pub const DG1: Probe = new_probe(23, ProbeType::DUPLICATOR, ProbeGrade::I, 0.0, 0.0, 0.0);
pub const PROBE_SET: [Probe; 24] = [
    B, MG1, MG2, MG3, MG4, MG5, MG6, MG7, MG8, MG9, MG10, RG1, RG2, RG3, RG4, RG5, RG6, RG7, RG8,
    RG9, RG10, BG1, BG2, DG1,
];

const fn new_probe(
    id: u16,
    probe_type: ProbeType,
    probe_grade: ProbeGrade,
    production: f32,
    revenue: f32,
    boost: f32,
) -> Probe {
    Probe {
        id,
        probe_type,
        probe_grade,
        production,
        revenue,
        boost,
    }
}
