use crate::{
    v1::constants::{parse_probe_grade, parse_probe_type, ProbeGrade, ProbeType, B, PROBE_SET},
    v1::graph::{Edge, Graph, Probe, Vertex},
};

use std::fs;

/// Reads file
///
/// Returns a graph
pub fn build_graph_from_file(file_path: &str) -> Graph {
    let mut v: Vec<Vertex> = Vec::new();
    let mut e: Vec<Edge> = Vec::new();

    // Read file
    let contents = fs::read_to_string(file_path).expect("Should have been able to read the file");

    // Split file into vertices and edges
    let (vertices, edges) = contents.split_at(contents.find("edges").unwrap());

    // Parse vertices
    vertices
        .lines()
        .skip(1)
        .filter(|line: &&str| !line.is_empty() && !line.starts_with("//"))
        .for_each(|vertex| v.push(parse_vertex(vertex)));

    edges
        .lines()
        .skip(1)
        .filter(|line| !line.is_empty() && !line.starts_with("//"))
        .for_each(|edge| e.push(parse_edge(edge)));

    // println!("Built graph: {:?}", graph);
    Graph {
        vertices: v,
        edges: e,
    }
}

fn parse_vertex(vertex: &str) -> Vertex {
    let vertex_fields: Vec<&str> = vertex.split(',').collect();
    let production: char = vertex_fields[1].chars().next().unwrap_or('C');
    let revenue: char = vertex_fields[2].chars().next().unwrap_or('F');
    Vertex {
        id: str::parse::<u16>(vertex_fields[0]).unwrap_or(0),
        production,
        revenue,
        probe: B,
    }
}

fn parse_edge(edge: &str) -> Edge {
    let (from, to) = edge.split_once(',').unwrap();

    Edge {
        from: str::parse::<u16>(from).unwrap_or(0),
        to: str::parse::<u16>(to).unwrap_or(0),
    }
}

/// Reads file
///
/// Returns a vector of Probes
pub fn build_probes_from_file(file_path: &str) -> Vec<Probe> {
    let contents: String =
        fs::read_to_string(file_path).expect("Should have been able to read the file");

    return contents
        .lines()
        .filter(|line| !line.is_empty() && !line.starts_with("//"))
        .map(|line: &str| {
            parse_probe(line)
        })
        .collect();
}

/// Parses Probe struct from a text
fn parse_probe(line: &str) -> Probe {
    // println!("Probe: {:?}", line);
    let (probe_type, probe_grade) = line
        .split_once('G')
        .unwrap_or_else(|| panic!("Invalid probe line"));
    let probe_type: ProbeType = parse_probe_type(probe_type);
    let probe_grade: ProbeGrade = parse_probe_grade(probe_grade);
    return PROBE_SET
        .iter()
        .find(|p| {
            // println!(
            //     "Checking...{:?} == {:?}, {:?} == {:?}",
            //     p.probe_type, probe_type, p.probe_grade, probe_grade
            // );
            p.probe_type == probe_type && p.probe_grade == probe_grade
        })
        .unwrap_or_else(|| panic!("Fuck this this"))
        .to_owned();
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::v1::constants::{ProbeGrade, ProbeType};

    #[test]
    fn parse_vertex_test() {
        let actual: Vertex = parse_vertex("100,A,A");

        assert_eq!("FN100", format!("FN{}", actual.id));
        assert_eq!('A', actual.production);
        assert_eq!('A', actual.revenue);
        assert_eq!(ProbeType::BASIC, actual.probe.probe_type);
        assert_eq!(ProbeGrade::I, actual.probe.probe_grade);
        assert_eq!(0.5, actual.probe.production);
        assert_eq!(0.5, actual.probe.revenue);
    }

    #[test]
    fn parse_edge_test() {
        let actual: Edge = parse_edge("1,2");

        assert_eq!(1, actual.from);
        assert_eq!(2, actual.to);
    }

    #[test]
    fn build_graph_from_file_1_test() {
        let actual = build_graph_from_file("examples/graph-1.txt");

        assert_eq!(3, actual.vertices.len());
        assert_eq!(2, actual.edges.len());
        assert_eq!("FN100", format!("FN{}", actual.vertices[0].id));
        assert_eq!("FN101", format!("FN{}", actual.vertices[1].id));
        assert_eq!("FN102", format!("FN{}", actual.vertices[2].id));
        assert_eq!('A', actual.vertices[0].production);
        assert_eq!('F', actual.vertices[0].revenue);

        assert_eq!('C', actual.vertices[1].production);
        assert_eq!('E', actual.vertices[1].revenue);

        assert_eq!('B', actual.vertices[2].production);
        assert_eq!('D', actual.vertices[2].revenue);
    }

    #[test]
    fn build_graph_from_file_primordia_test() {
        let actual = build_graph_from_file("examples/graph-primordia.txt");

        assert_eq!(20, actual.vertices.len());
        assert_eq!(19, actual.edges.len());
    }

    #[test]
    fn parse_probe_test() {
        let expected = Probe {
            id: 1,
            probe_type: ProbeType::MINING,
            probe_grade: crate::v1::constants::ProbeGrade::I,
            production: 1.0,
            revenue: 0.3,
            boost: 0.0,
        };
        let actual = parse_probe("MG1");

        assert_eq!(actual.probe_type, expected.probe_type);
        assert_eq!(actual.production, expected.production);
        assert_eq!(actual.revenue, expected.revenue);
    }

    #[test]
    fn parse_probe_bg2_test() {
        let expected = Probe {
            id: 22,
            probe_type: ProbeType::BOOST,
            probe_grade: crate::v1::constants::ProbeGrade::II,
            production: 0.3,
            revenue: 0.3,
            boost: 2.0,
        };
        let actual = parse_probe("BG2");

        assert_eq!(actual.probe_type, expected.probe_type);
        assert_eq!(actual.production, expected.production);
        assert_eq!(actual.revenue, expected.revenue);
    }

    #[test]
    fn build_probes_from_file_1_test() {
        let actual: Vec<Probe> = build_probes_from_file("examples/probes-1.txt");

        assert_eq!(2, actual.len());
        assert_eq!(ProbeType::MINING, actual[0].probe_type);
        assert_eq!(ProbeGrade::I, actual[0].probe_grade);
        assert_eq!(ProbeType::MINING, actual[1].probe_type);
        assert_eq!(ProbeGrade::II, actual[1].probe_grade);
    }

    #[test]
    fn build_probes_from_file_2_test() {
        let actual: Vec<Probe> = build_probes_from_file("examples/probes-2.txt");

        assert_eq!(4, actual.len());
        assert_eq!(ProbeType::MINING, actual[0].probe_type);
        assert_eq!(ProbeGrade::I, actual[0].probe_grade);
        assert_eq!(ProbeType::MINING, actual[1].probe_type);
        assert_eq!(ProbeGrade::II, actual[1].probe_grade);
        assert_eq!(ProbeType::REVENUE, actual[2].probe_type);
        assert_eq!(ProbeGrade::X, actual[2].probe_grade);
        assert_eq!(ProbeType::BOOST, actual[3].probe_type);
        assert_eq!(ProbeGrade::I, actual[3].probe_grade);
    }
}
