use std::collections::HashSet;

use crate::v1::constants::{ProbeGrade, ProbeType, B, RATES};

/// probe_type: Represents type and grade.
/// e.g. MG1-10,RG1-10,BG1-2,DG1
///
/// production and revenue: Rate of resource generation
/// e.g. 2.2 and 0.3, 0.3 and 100
#[derive(Debug, Clone)]
pub struct Probe {
    pub id: u16,
    pub probe_type: ProbeType,
    pub probe_grade: ProbeGrade,
    pub production: f32,
    pub revenue: f32,
    pub boost: f32,
}
impl Ord for Probe {
    fn cmp(&self, other: &Self) -> Ordering {
        other.id.cmp(&self.id)
    }
}
impl PartialOrd for Probe {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl PartialEq for Probe {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}
impl Eq for Probe {}

/// id: Unique identifier
/// e.g. 101,224,400
#[derive(Clone, Debug)]
pub struct Vertex {
    pub id: u16,
    pub production: char,
    pub revenue: char,
    pub probe: Probe,
}
impl Vertex {
    pub fn new(id: u16, production: char, revenue: char) -> Self {
        Self {
            id,
            production,
            revenue,
            probe: B,
        }
    }
}

/// from and to: vertices ids
#[derive(Clone, Debug)]
pub struct Edge {
    pub from: u16,
    pub to: u16,
}

#[derive(Clone, Debug)]
pub struct Graph {
    pub vertices: Vec<Vertex>,
    pub edges: Vec<Edge>,
}
impl Graph {
    pub fn new(v: Vec<Vertex>, e: Vec<Edge>) -> Graph {
        Graph {
            vertices: v,
            edges: e,
        }
    }
    pub fn total_production(&self) -> f32 {
        let total_production = self.vertices.iter().fold(0.0, |acc, v| {
            let a = self.compute_production(v.id);
            // println!("Compute production value: {}", a);
            acc + a
        });
        total_production
    }

    fn compute_production(&self, vertex_id: u16) -> f32 {
        let vertex = self
            .vertices
            .iter()
            .find(|v| v.id == vertex_id)
            .unwrap_or_else(|| panic!("Requested compute production of non-existent vertex"));

        let base_production = RATES
            .iter()
            .find(|rate| rate.0.eq("M") && rate.1 == vertex.production)
            .unwrap()
            .2;

        let adjacent_vertices: Vec<Vertex> = self.get_adjacent_vertices(vertex_id);

        let vertex_local_production = match vertex.probe.probe_type {
            ProbeType::DUPLICATOR => {
                base_production * self.mimic_adjacent_production(&adjacent_vertices)
            }
            _ => {
                base_production * vertex.probe.production
            }
        };

        // For each adjacent probe, if its BOOSTER, add and multiply
        let adjacent_boosters_production = self.get_boost_from_booster(&adjacent_vertices);

        // For each adjacent probe, if its DUPLICATOR, check if has boosters close
        let adjacent_boosters_by_duplicators_production =
            self.get_boost_from_duplicators(&adjacent_vertices);

        vertex_local_production
            * adjacent_boosters_production
            * adjacent_boosters_by_duplicators_production
    }

    fn get_adjacent_vertices(&self, vertex_id: u16) -> Vec<Vertex> {
        let adjacent_vertex_set = HashSet::new();
        let adjacent_vertex_ids: Vec<Vertex> = self
            .edges
            .iter()
            .filter(|e| e.from == vertex_id || e.to == vertex_id)
            .fold(adjacent_vertex_set, |mut acc, e| {
                if e.from != vertex_id {
                    acc.insert(e.from);
                }
                if e.to != vertex_id {
                    acc.insert(e.to);
                }
                acc
            })
            .iter()
            .map(|v_id| {
                return self.find_vertex_by_id(*v_id).clone();
            })
            .collect();
        adjacent_vertex_ids
    }

    fn mimic_adjacent_production(&self, adjacent_vertices: &[Vertex]) -> f32 {
        adjacent_vertices.iter().fold(0.0, |acc, cur| {
            acc + cur.probe.production
        })
    }

    fn find_vertex_by_id(&self, v_id: u16) -> &Vertex {
        for v in &self.vertices {
            if v.id == v_id {
                return v;
            }
        }
        panic!("vertex with id {} not found", v_id);
    }

    /// TODO: Should get vertex id and calculate adyacent vertices by itself
    fn get_boost_from_booster(&self, adjacent_vertices: &[Vertex]) -> f32 {
        return adjacent_vertices
            .iter()
            .filter(|v| v.probe.probe_type == ProbeType::BOOST)
            .fold(1.0, |acc, v| acc * v.probe.boost);
    }

    fn get_boost_from_duplicators(&self, adjacent_vertices: &[Vertex]) -> f32 {
        return adjacent_vertices
            .iter()
            .filter(|v| v.probe.probe_type == ProbeType::DUPLICATOR)
            .fold(1.0, |acc, v| {
                acc * self.get_boost_from_booster(&self.get_adjacent_vertices(v.id))
            });
    }
}

#[cfg(test)]
mod tests {

    use crate::{
        v1::constants::{BG1, BG2, DG1, MG1, MG10, MG2, MG3, MG7, RG10},
        v1::test_utils,
    };

    use super::*;

    #[test]
    fn compute_production_test() {
        let vertices_test_set = vec![
            vec![B.id],
            vec![MG1.id],
            vec![RG10.id],
            vec![MG7.id, BG1.id],
            vec![MG10.id, BG1.id, BG2.id],
            vec![MG1.id, DG1.id, BG2.id],
            vec![MG1.id, BG2.id, DG1.id, BG1.id],
            vec![BG2.id, BG1.id],
            vec![DG1.id, BG2.id],
            vec![BG2.id, DG1.id],
        ];
        let edges_test_set = vec![
            vec![(0, 0)],
            vec![(0, 0)],
            vec![(0, 0)],
            vec![(0, 1)],
            vec![(0, 1), (0, 2)],
            vec![(0, 1), (1, 2)],
            vec![(0, 3), (1, 2), (2, 0)],
            vec![(0, 1)],
            vec![(0, 1)],
            vec![(0, 1)],
        ];
        let expected_test_set = [
            250.0,
            500.0,
            150.0,
            500.0 * 2.2 * 1.5,
            500.0 * 3.0 * 1.5 * 2.0,
            500.0 * 1.0 * 2.0,
            500.0 * 1.0 * 2.0 * 1.5,
            500.0 * 0.3 * 1.5,
            500.0 * 0.3 * 2.0,
            500.0 * 0.3 * 2.0,
        ];

        let mut i = 0;
        while i < vertices_test_set.len() {
            let v: Vec<Vertex> = test_utils::create_vertices(&vertices_test_set[i]);
            let e = test_utils::create_edges(&edges_test_set[i], &v);
            let g1 = Graph::new(v, e);

            let actual = g1.compute_production(g1.vertices[0].id);

            assert_eq!(expected_test_set[i], actual, "Case {}", i);

            i += 1;
        }
    }

    #[test]
    fn total_production_of_mg1_and_mg1_test() {
        let id1 = 100;
        let id2 = 101;
        let mut v1: Vertex = Vertex::new(id1, 'B', 'F');
        v1.probe = MG1;
        let mut v2: Vertex = Vertex::new(id2, 'C', 'E');
        v2.probe = MG1;
        let e1: Edge = Edge { from: id1, to: id2 };
        let g1 = Graph {
            vertices: vec![v1, v2],
            edges: vec![e1],
        };

        let actual = g1.total_production();

        assert_eq!(350.0 * 1.0 + 250.0 * 1.0, actual);
    }

    #[test]
    fn total_production_of_bg2_mg1_and_bg1_test() {
        let id1 = 102;
        let id2 = 103;
        let id3 = 104;
        let mut v1: Vertex = Vertex::new(id1, 'A', 'A');
        v1.probe = BG2;
        let mut v2: Vertex = Vertex::new(id2, 'A', 'A');
        v2.probe = MG1;
        let mut v3: Vertex = Vertex::new(id3, 'C', 'C');
        v3.probe = BG1;
        let e1: Edge = Edge { from: id1, to: id2 };
        let e2: Edge = Edge { from: id2, to: id3 };
        let g1 = Graph {
            vertices: vec![v1, v2, v3],
            edges: vec![e1, e2],
        };

        let actual = g1.total_production();

        assert_eq!(500.0 * 1.0 * 2.0 * 1.5 + 500.0 * 0.3 + 250.0 * 0.3, actual);
    }

    #[test]
    fn total_production_of_bg1_dg1_bg2_test() {
        let v: Vec<Vertex> = test_utils::create_vertices(&vec![DG1.id, BG2.id]);
        let e = test_utils::create_edges(&[(0, 1)], &v);
        let g1 = Graph::new(v, e);

        let actual = g1.total_production();

        assert_eq!((500.0 * 0.3 * 2.0) + (500.0 * 0.3 * 2.0), actual);
    }

    #[test]
    fn total_production_of_mg10_dg1_test() {
        let v: Vec<Vertex> = test_utils::create_vertices(&vec![DG1.id, MG10.id]);
        let e = test_utils::create_edges(&[(0, 1)], &v);
        let g1 = Graph::new(v, e);

        let actual = g1.total_production();

        assert_eq!((500.0 * 3.0) + (500.0 * 3.0), actual);
    }

    #[test]
    fn total_production_of_bg2_mg10_dg1_test() {
        let v: Vec<Vertex> = test_utils::create_vertices(&vec![BG2.id, DG1.id, MG10.id]);
        let e = test_utils::create_edges(&[(0, 1), (1, 2)], &v);
        let g1 = Graph::new(v, e);

        let actual = g1.total_production();

        assert_eq!(
            (500.0 * 0.3 * 2.0) + (500.0 * (0.3 + 3.0) * 2.0) + (500.0 * 3.0 * 2.0),
            actual
        );
    }

    #[test]
    fn total_production_of_bg2_dg1_mg1_and_bg1_test() {
        let v: Vec<Vertex> = test_utils::create_vertices(&vec![BG2.id, DG1.id, MG1.id, BG1.id]);
        let e = test_utils::create_edges(&[(0, 1), (1, 2), (2, 3)], &v);
        let g1 = Graph::new(v, e);

        let actual = g1.total_production();

        assert_eq!(
            (500.0 * 0.3 * 2.0)
                + (500.0 * (1.0 + 0.3) * 2.0)
                + (500.0 * 1.0 * 2.0 * 1.5)
                + (500.0 * 0.3),
            actual
        );
    }

    #[test]
    fn total_production_of_same_probes_test() {
        let v: Vec<Vertex> = test_utils::create_vertices(&vec![MG1.id, MG1.id]);
        let e = test_utils::create_edges(&[(0, 1)], &v);
        let g1 = Graph::new(v, e);

        let actual = g1.total_production();

        assert_eq!((500.0) + (500.0), actual);
    }

    #[test]
    fn get_adjacent_vertices_test() {
        let vertices = test_utils::create_vertices(&vec![B.id, MG1.id, B.id, B.id, B.id]);
        let edges = test_utils::create_edges(&[(0, 1), (1, 2), (1, 3)], &vertices);
        let g1 = Graph { vertices, edges };

        let adyacent_vertices = g1.get_adjacent_vertices(g1.vertices[1].id);

        assert_eq!(3, adyacent_vertices.len());
        let v0 = adyacent_vertices
            .iter()
            .find(|a| a.id == g1.vertices[0].id)
            .unwrap()
            .id;
        assert_eq!(g1.vertices[0].id, v0);
        let v2 = adyacent_vertices
            .iter()
            .find(|a| a.id == g1.vertices[2].id)
            .unwrap()
            .id;
        assert_eq!(g1.vertices[2].id, v2);
        let v3 = adyacent_vertices
            .iter()
            .find(|a| a.id == g1.vertices[3].id)
            .unwrap()
            .id;
        assert_eq!(g1.vertices[3].id, v3);
    }

    #[test]
    fn find_vertex_by_id_test() {
        let vertices = test_utils::create_vertices(&vec![B.id, MG1.id, B.id, B.id, B.id]);
        let edges = test_utils::create_edges(&[(0, 1), (1, 2), (1, 3)], &vertices);
        let g1 = Graph { vertices, edges };

        let found_vertex = g1.find_vertex_by_id(101);

        assert_eq!(101, found_vertex.id);
        assert_eq!(ProbeType::MINING, found_vertex.probe.probe_type);
        assert_eq!(ProbeGrade::I, found_vertex.probe.probe_grade);
    }

    #[test]
    fn get_total_boost_test() {
        let vertices = test_utils::create_vertices(&vec![BG1.id, BG2.id]);
        let edges = test_utils::create_edges(&[(0, 1)], &vertices);
        let g1 = Graph { vertices, edges };

        let total_boost = g1.get_boost_from_booster(&g1.vertices);

        assert_eq!(3.0, total_boost);
    }

    #[test]
    fn get_boost_from_duplicators_test() {
        let vertices = test_utils::create_vertices(&vec![MG1.id, DG1.id, BG1.id, BG2.id]);
        let edges = test_utils::create_edges(&[(0, 1), (1, 2), (1, 3)], &vertices);
        let g1 = Graph { vertices, edges };

        // This is bad. Don't contaminate tests with other functions
        let adyacent_vertices = g1.get_adjacent_vertices(g1.vertices[0].id);
        let total_boost = g1.get_boost_from_duplicators(&adyacent_vertices);

        assert_eq!(3.0, total_boost);
    }

    #[test]
    fn sort_probes_test() {
        let mut actual: Vec<Probe> = vec![
            MG1, MG3, MG1, MG1, RG10, MG2, MG1, MG1, RG10, MG1, BG2, MG1, MG1, MG10, MG1, MG1, MG1,
            MG10, BG1, BG2, DG1,
        ];

        let expected: Vec<Probe> = vec![
            DG1, BG2, BG2, BG1, RG10, RG10, MG10, MG10, MG3, MG2, MG1, MG1, MG1, MG1, MG1, MG1,
            MG1, MG1, MG1, MG1, MG1,
        ];

        actual.sort();

        expected.iter().enumerate().for_each(|(i, _)| {
            assert_eq!(expected[i].id, actual[i].id, "On expected position {}", i);
        })
    }
}
