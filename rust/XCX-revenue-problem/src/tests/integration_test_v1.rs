#[cfg(test)]
mod integration_test_v1 {
    use crate::{v1::{graph::{Graph, Probe}, parser::{build_graph_from_file, build_probes_from_file}, problem::find_highest_production}, tests::common::setup};


    #[test]
    fn small_graph_mining_probes_ex1() {
        setup();
    
        let graph: Graph = build_graph_from_file("examples/graph-1.txt");
        let probes_list: Vec<Probe> = build_probes_from_file("examples/probes-1.txt");
        let solution = vec![];
    
        let actual_highest_revenue = find_highest_production(&graph, &probes_list, &solution);
        println!(
            "small_graph_mining_probes_ex1 solution: {:?}",
            actual_highest_revenue.2
        );
        let expected_highest_revenue = (500.0 * 1.2) + (250.0 * 0.5) + (350.0 * 1.0);
    
        assert_eq!(expected_highest_revenue, actual_highest_revenue.3);
    }
    
    #[test]
    fn small_graph_mining_probes_ex2() {
        setup();
    
        let graph: Graph = build_graph_from_file("examples/graph-2.txt");
        let probes_list: Vec<Probe> = build_probes_from_file("examples/probes-2.txt");
        let solution = vec![];
    
        let actual_highest_revenue = find_highest_production(&graph, &probes_list, &solution);
    
        let expected_highest_revenue =
            (500.0 * 1.0 * 1.5) + (500.0 * 0.3) + (350.0 * 0.3 * 1.5) + (500.0 * 1.2 * 1.5);
    
        assert_eq!(expected_highest_revenue, actual_highest_revenue.3);
        // MG1 in 1
        // BG1 in 2
        // RG10 in 3
        // MG2 in 4
    }
    
    #[test]
    fn small_graph_mining_probes_ex3() {
        setup();
    
        let graph: Graph = build_graph_from_file("examples/graph-3.txt");
        let probes_list: Vec<Probe> = build_probes_from_file("examples/probes-3.txt");
        let solution = vec![];
    
        let actual_highest_revenue = find_highest_production(&graph, &probes_list, &solution);
    
        let expected_highest_revenue =
            250.0 * 0.5 * 2.0 + 250.0 * 3.0 * 2.0 + 250.0 * 0.3 + 250.0 * 0.5 + 500.0 * 3.0 * 2.0;
    
        assert_eq!(expected_highest_revenue, actual_highest_revenue.3);
        // MG1 in 1
        // BG1 in 2
        // RG10 in 3
        // MG2 in 4
    }
    
    #[test]
    fn primordia_with_ex_probes_3_test() {
        setup();
    
        let graph: Graph = build_graph_from_file("examples/graph-primordia.txt");
        let probes_list: Vec<Probe> = build_probes_from_file("examples/probes-3.txt");
        let solution = vec![];
    
        let actual_highest_revenue = find_highest_production(&graph, &probes_list, &solution);
    
        println!(
            "highest graph configuration: {:?}",
            actual_highest_revenue.2
        );
    
        let expected_highest_revenue = 8925.0;
    
        assert_eq!(expected_highest_revenue, actual_highest_revenue.3);
        // MG1 in 1
        // BG1 in 2
        // RG10 in 3
        // MG2 in 4
    }
    
    #[test]
    // #[ignore = "Not finishing"]
    // #[ignore = "Tooks 13.10s and 362k check of total_production at 10-20ums"]
    /// Tooks 13.10s and 362k check of total_production at 10-20ums
    fn primordia_half_e2e_test() {
        setup();
    
        let graph: Graph = build_graph_from_file("examples/graph-primordia-half.txt");
        let probes_list: Vec<Probe> = build_probes_from_file("examples/probes-inventory-half.txt");
        let solution = vec![];
    
        let actual_highest_revenue = find_highest_production(&graph, &probes_list, &solution);
    
        println!(
            "highest graph configuration: {:?}",
            actual_highest_revenue.2
        );
    
        // [
        //     SolutionPair { vertex_id: 101, probe_id: MG8 },
        //     SolutionPair { vertex_id: 102, probe_id: DG1 },
        //     SolutionPair { vertex_id: 103, probe_id: BG2 },
        //     SolutionPair { vertex_id: 104, probe_id: BG2 },
        //     SolutionPair { vertex_id: 105, probe_id: DG1 },
        //     SolutionPair { vertex_id: 106, probe_id: DG1 },
        //     SolutionPair { vertex_id: 107, probe_id: MG9 },
        //     SolutionPair { vertex_id: 108, probe_id: BG1 },
        //     SolutionPair { vertex_id: 109, probe_id: MG9 }
        // ]
        let expected_highest_revenue = (250.0 * 2.4) // MG8  in 1
            + (250.0 * 2.7 * 1.5)// MG9  in 2
            + (250.0 * 0.3 * 1.5)// BG2  in 3
            + (250.0 * 0.3 * 1.5 * 2.0)// BG1  in 4
            + (500.0 * 2.7 * 2.0 * 2.0)// MG9  in 5
            + (350.0 * (3.0 + 0.3 + 0.3) * 1.5 * 2.0)// DG1  in 6
            + (500.0 * 3.0 * 1.5 * 2.0)// MG10 in 7
            + (250.0 * 0.3 * 2.0) // BG2  in 8
            + (250.0 * (2.7 + 0.3) * 2.0); // DG1  in 9
    
        assert_eq!(expected_highest_revenue, actual_highest_revenue.3);
    }
    
    #[test]
    // #[ignore = "Not finishing"]
    fn primordia_e2e_v1_test() {
        setup();
    
        let graph: Graph = build_graph_from_file("examples/graph-primordia.txt");
        let probes_list: Vec<Probe> = build_probes_from_file("examples/probes-inventory-purged.txt");
        let solution = vec![];
    
        let actual_highest_revenue = find_highest_production(&graph, &probes_list, &solution);
    
        println!("highest graph configuration: {:?}", actual_highest_revenue);
    
        let expected_highest_revenue = 1.0;
    
        assert_eq!(expected_highest_revenue, actual_highest_revenue.3);
        // MG1 in 1
        // BG1 in 2
        // RG10 in 3
        // MG2 in 4
    }
    
}
