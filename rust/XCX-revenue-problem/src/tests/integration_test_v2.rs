#[cfg(test)]
mod integration_test_v2 {
    use std::collections::HashMap;

    use fnv::FnvHashMap;

    use crate::{
        tests::common::setup,
        v2::{
            graph::Graph,
            parser::{build_graph_from_file, build_probes_from_file},
            probe::Probe,
            vertex::Vertex,
        },
    };

    #[test]
    fn e2e_1_test() {
        setup();

        let graph: Graph<String, Vertex> = build_graph_from_file("examples/graph-1.txt");
        let probes_placed_parsed: HashMap<u16, Probe> =
            build_probes_from_file("examples/probes-1.txt");

        let probes_placed: FnvHashMap<String, u16> = FnvHashMap::default();
        let probes_available: Vec<u16> = probes_placed_parsed
            .iter()
            .map(|p| *p.0)
            .collect::<Vec<u16>>();
        let mut probes_definition: FnvHashMap<u16, Probe> = FnvHashMap::default();
        probes_placed_parsed.iter().for_each(|(k, v)| {
            probes_definition.insert(*k, v.clone());
        });
        let mut combinations_checked: HashMap<String, u16> = HashMap::new();

        let expected_highest_revenue = 1075.0;
        let actual_highest_revenue = graph.find_highest_production_subgraph(
            'M',
            probes_placed,
            &probes_available,
            &probes_definition,
            &mut combinations_checked,
        );

        assert_eq!(expected_highest_revenue, actual_highest_revenue.0);
        assert_eq!(0, *actual_highest_revenue.1.get("102").unwrap());
        assert_eq!(1, *actual_highest_revenue.1.get("100").unwrap());
    }

    // Greedy algorithm: 59049 leafs
    // Greedy + purge by hash: 15120
    #[test]
    fn e2e_2_test() {
        setup();

        let graph: Graph<String, Vertex> = build_graph_from_file("examples/graph-2.txt");
        let probes_placed_parsed: HashMap<u16, Probe> =
            build_probes_from_file("examples/probes-2.txt");

        let probes_placed: FnvHashMap<String, u16> = FnvHashMap::default();
        let probes_available = probes_placed_parsed
            .iter()
            .map(|p| *p.0)
            .collect::<Vec<u16>>();
        let mut probes_definition: FnvHashMap<u16, Probe> = FnvHashMap::default();
        probes_placed_parsed.iter().for_each(|(k, v)| {
            probes_definition.insert(*k, v.clone());
        });
        let mut combinations_checked: HashMap<String, u16> = HashMap::new();

        let expected_highest_revenue =
            (500.0 * 1.0 * 1.5) + (500.0 * 0.3) + (350.0 * 0.3 * 1.5) + (500.0 * 1.2 * 1.5);
        let actual_highest_revenue = graph.find_highest_production_subgraph(
            'M',
            probes_placed,
            &probes_available,
            &probes_definition,
            &mut combinations_checked,
        );
        println!("Solution: {:#?}", actual_highest_revenue);

        assert_eq!(expected_highest_revenue, actual_highest_revenue.0);
        assert_eq!(0, *actual_highest_revenue.1.get("100").unwrap());
        assert_eq!(1, *actual_highest_revenue.1.get("103").unwrap());
        assert_eq!(2, *actual_highest_revenue.1.get("102").unwrap());
        assert_eq!(3, *actual_highest_revenue.1.get("101").unwrap());
    }

}
