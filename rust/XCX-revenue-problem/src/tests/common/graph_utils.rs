use fnv::FnvHashMap;

use crate::v2::{
    constants::{BG1, DG1, MG1, MG10},
    graph::Graph,
    probe::Probe,
    vertex::Vertex,
};

pub fn create_vertex(id: String, production: char, revenue: char) -> Vertex {
    Vertex {
        id,
        production,
        revenue,
    }
}

pub fn create_graph<'a>(
    vertices: u16,
) -> (
    Graph<&'a str, Vertex>,
    FnvHashMap<&'a str, u16>,
    FnvHashMap<u16, Probe>,
) {
    match vertices {
        10 => {
            let mut graph: Graph<&str, Vertex> = Graph::new();
            let mut start_id = 100;
            let a = [
                ("100", 'B', 'F'),
                ("101", 'A', 'E'),
                ("102", 'C', 'E'),
                ("103", 'C', 'E'),
                ("104", 'A', 'E'),
                ("105", 'B', 'E'),
                ("106", 'C', 'E'),
                ("107", 'A', 'E'),
                ("108", 'B', 'E'),
                ("109", 'C', 'E'),
            ];
            for (id, production, revenue) in a {
                graph.push_vertex(id, create_vertex(id.to_string(), production, revenue));
            }
            
            graph.push_edge("100", "101");
            graph.push_edge("100", "102");
            graph.push_edge("101", "102");
            graph.push_edge("102", "103");

            // Probes placed: Init state: Best solution
            let mut probes_placed: FnvHashMap<&str, u16> = FnvHashMap::default();
            probes_placed.insert("100", 4);
            probes_placed.insert("101", 3);
            probes_placed.insert("102", 1);
            probes_placed.insert("103", 2);

            // Probes definition
            let mut probes_definition: FnvHashMap<u16, Probe> = FnvHashMap::default();
            probes_definition.insert(1, DG1);
            probes_definition.insert(2, BG1);
            probes_definition.insert(3, MG10);
            probes_definition.insert(4, MG1);

            return (graph, probes_placed, probes_definition);
        }
        _ => {
            let mut graph: Graph<&str, Vertex> = Graph::new();
            graph.push_vertex(
                "100",
                Vertex {
                    id: String::from("100"),
                    production: 'B',
                    revenue: 'F',
                },
            );
            graph.push_vertex(
                "101",
                Vertex {
                    id: String::from("101"),
                    production: 'A',
                    revenue: 'E',
                },
            );
            graph.push_vertex(
                "102",
                Vertex {
                    id: String::from("102"),
                    production: 'C',
                    revenue: 'E',
                },
            );
            graph.push_vertex(
                "103",
                Vertex {
                    id: String::from("103"),
                    production: 'C',
                    revenue: 'E',
                },
            );
            graph.push_edge("100", "101");
            graph.push_edge("100", "102");
            graph.push_edge("101", "102");
            graph.push_edge("102", "103");

            // Probes placed: Init state: Best solution
            let mut probes_placed: FnvHashMap<&str, u16> = FnvHashMap::default();
            probes_placed.insert("100", 4);
            probes_placed.insert("101", 3);
            probes_placed.insert("102", 1);
            probes_placed.insert("103", 2);

            // Probes definition
            let mut probes_definition: FnvHashMap<u16, Probe> = FnvHashMap::default();
            probes_definition.insert(1, DG1);
            probes_definition.insert(2, BG1);
            probes_definition.insert(3, MG10);
            probes_definition.insert(4, MG1);

            return (graph, probes_placed, probes_definition);
        }
    }
}
