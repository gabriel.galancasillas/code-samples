use super::constants::{ProbeGrade, ProbeType};

#[derive(Debug, Clone)]
pub struct Probe {
    pub id: u16,                 // Unique. Should be Box<str>?
    pub probe_type: ProbeType,   // Perhaps it's not needed
    pub probe_grade: ProbeGrade, // Perhaps it's not needed
    pub production_mult: f32,
    pub revenue_mult: f32,
    pub boost: f32,
}

