use std::borrow::Borrow;
use std::fmt::Display;
use std::hash::Hash;
use std::{collections::HashMap, fmt::Debug};

use fnv::FnvHashMap;

use crate::v1::graph;
use crate::v2::constants::{ProbeType, MG1};

use super::constants::{find_production_rate, B};
use super::probe::Probe;
use super::vertex::{Vertex, VertexTrait};

#[derive(Clone, Debug)]
pub struct Graph<VId, V> {
    pub vertices: FnvHashMap<VId, V>,
    pub edges: FnvHashMap<VId, Vec<VId>>,
}
impl<VId, V> Graph<VId, V>
where
    VId: Debug + Clone + Eq + Hash + Borrow<VId> + Display + Ord,
    V: Clone + Debug + VertexTrait,
{
    pub fn new() -> Graph<VId, V> {
        Graph {
            vertices: FnvHashMap::default(),
            edges: FnvHashMap::default(),
        }
    }

    pub fn push_vertex(self: &mut Graph<VId, V>, vid: VId, v: V) {
        self.vertices.insert(vid, v);
    }

    pub fn push_edge(self: &mut Graph<VId, V>, from: VId, to: VId) {
        let edge = self.edges.entry(from.clone()).or_default();
        edge.push(to.clone());
        let edge = self.edges.entry(to).or_default();
        edge.push(from);
    }

    pub fn set_vertices(self: &mut Graph<VId, V>, vertices: FnvHashMap<VId, V>) {
        self.vertices = vertices;
    }
    pub fn set_edges(self: &mut Graph<VId, V>, edges: FnvHashMap<VId, Vec<VId>>) {
        self.edges = edges;
    }
    
    pub fn find_highest_production(
        &self,
        production_type: char,
        probes_placed: FnvHashMap<VId, u16>,
        probes_available: &[u16],
        probes_definition: &FnvHashMap<u16, Probe>,
        combinations_checked: &mut HashMap<String, u16>,
    ) -> (f32, FnvHashMap<VId, u16>) {
        let mut a: (f32, FnvHashMap<VId, u16>) = (0.0, FnvHashMap::default());
        let mut probes_placed_acc = probes_placed;
        let mut i = 0;
        while probes_placed_acc.len() != probes_available.len()
            && probes_placed_acc.len() != self.vertices.len() && i < 10
        {
            i = i + 1;
            println!("While iteration. Probes left: {}", probes_available.len() - probes_placed_acc.len());
            println!("probes_placed_acc: {:#?}", probes_placed_acc);
            // Find biggest cluster with depth 2
            let edges: FnvHashMap<VId, Vec<VId>> = self.find_biggest_cluster();
            let mut graph: Graph<VId, V> = self.clone();
            graph.set_edges(edges);

            // Test N probes where N is number of
            a = graph.find_highest_production_subgraph(
                production_type,
                probes_placed_acc,
                probes_available,
                probes_definition,
                combinations_checked,
            );
            println!("subgraph output: {:#?}", a);

            probes_placed_acc = a.1.clone();
        }

        a
    }

    pub fn find_highest_production_subgraph(
        &self,
        production_type: char,
        probes_placed: FnvHashMap<VId, u16>,
        probes_available: &[u16],
        probes_definition: &FnvHashMap<u16, Probe>,
        combinations_checked: &mut HashMap<String, u16>,
    ) -> (f32, FnvHashMap<VId, u16>) {
        println!("probes_available: {:#?}", probes_available);
        println!("probes_placed: {:#?}", probes_placed);
        println!("probes_definition: {:#?}", probes_definition);
        
        if probes_available.is_empty() || self.vertices.len() == probes_placed.len() {
            return (
                self.total_production(production_type, &probes_placed, probes_definition),
                probes_placed,
            );
        }

        return self
            .vertices
            .iter()
            .fold((f32::MIN, FnvHashMap::default()), |max, v| {
                // Debug
                if probes_placed.is_empty() {
                    println!(
                        "{}, {}, {:.2}%",
                        probes_placed.len(),
                        probes_definition.len(),
                        f32::powf(10.0, 2.0)
                            * (1.0 - probes_placed.len() as f32 / probes_definition.len() as f32)
                    );
                }

                if probes_placed.contains_key(v.0) {
                    return max;
                }
                let mut new_probes_placed = probes_placed.clone();
                new_probes_placed.insert(v.0.to_owned(), probes_available[0]);

                let solution_hash: String = get_hash::<VId>(&new_probes_placed, probes_definition);
                if combinations_checked.contains_key(&solution_hash) {
                    return max;
                } else {
                    combinations_checked.insert(solution_hash, 1);
                }

                let current_highest: (f32, FnvHashMap<VId, u16>);
                if probes_available.len() == 1 {
                    current_highest = self.find_highest_production_subgraph(
                        production_type,
                        new_probes_placed,
                        &[],
                        probes_definition,
                        combinations_checked,
                    )
                } else {
                    current_highest = self.find_highest_production_subgraph(
                        production_type,
                        new_probes_placed,
                        &probes_available[1..probes_available.len()],
                        probes_definition,
                        combinations_checked,
                    )
                }

                if current_highest.0 > max.0 {
                    current_highest
                } else {
                    max
                }
            });
    }

    fn find_biggest_cluster(&self) -> FnvHashMap<VId, Vec<VId>> {
        let mut sub_graph: FnvHashMap<VId, Vec<VId>> = FnvHashMap::default();

        // Get center of cluster: First VId with most adyacent vertices
        let mut center: VId = self.vertices.iter().next().unwrap().0.clone();
        let mut max_vertices = 0;
        self.edges.iter().for_each(|edge| {
            if edge.1.len() > max_vertices {
                max_vertices = edge.1.len();
                center = edge.0.clone();
            }
        });

        // Get adyacent and push them to new edges
        sub_graph.insert(center.clone(), self.edges.get(&center).unwrap().to_vec());
        self.edges.get(&center).unwrap().iter().for_each(|e1| {
            self.edges.get(e1).unwrap().iter().for_each(|e2| {
                sub_graph.insert(e2.clone(), self.edges.get(&e2).unwrap().to_vec());
            });
            sub_graph.insert(e1.clone(), self.edges.get(&e1).unwrap().to_vec());
        });

        sub_graph
    }

    fn total_production(
        &self,
        production_type: char,
        probes_placed: &FnvHashMap<VId, u16>,
        probes_definition: &FnvHashMap<u16, Probe>,
    ) -> f32 {
        self.vertices.iter().fold(0.0, |acc, curr| {
            let production =
                self.compute_production(production_type, curr.0, probes_placed, probes_definition);
            acc + production
        })
    }

    fn compute_production(
        &self,
        production_type: char,
        vid: &VId,
        probes_placed: &FnvHashMap<VId, u16>,
        probes_definition: &FnvHashMap<u16, Probe>,
    ) -> f32 {
        let vertex: &V = self.vertices.get(&vid).unwrap();
        let base_production = find_production_rate(production_type, vertex.get_production());

        // If duplicator -> probe production = ∑ adjacent_production_probes
        let vertex_local_production: f32;
        if self
            .get_probe_from_vid(vid, probes_placed, probes_definition)
            .probe_type
            == ProbeType::DUPLICATOR
        {
            vertex_local_production = base_production
                * self.copy_adjacent_productions(&vid, probes_placed, probes_definition);
        } else {
            vertex_local_production = base_production
                * self
                    .get_probe_from_vid(vid, probes_placed, probes_definition)
                    .production_mult;
        }

        // For each adjacent probe, if its BOOSTER, add and multiply
        let adjacent_boosters_production =
            self.get_boost_from_boosters(&vid, probes_placed, probes_definition);

        // For each adjacent probe, if its DUPLICATOR, check if has boosters close
        let adjacent_boosters_by_duplicators_production =
            self.get_boost_from_duplicators(vid, probes_placed, probes_definition);

        // println!(
        //     "{}: A ({}) * B ({}) * C ({})",
        //     vid,
        //     vertex_local_production,
        //     adjacent_boosters_production,
        //     adjacent_boosters_by_duplicators_production
        // );
        vertex_local_production
            * adjacent_boosters_production
            * adjacent_boosters_by_duplicators_production
    }

    fn copy_adjacent_productions(
        &self,
        vid: &VId,
        probes_placed: &FnvHashMap<VId, u16>,
        probes_definition: &FnvHashMap<u16, Probe>,
    ) -> f32 {
        return self.edges.get(&vid).unwrap().iter().fold(0.0, |acc, curr| {
            return acc
                + self
                    .get_probe_from_vid(curr, probes_placed, probes_definition)
                    .production_mult;
        });
    }

    fn get_probe_from_vid<'a>(
        &'a self,
        vid: &VId,
        probes_placed: &FnvHashMap<VId, u16>,
        probes_definition: &'a FnvHashMap<u16, Probe>,
    ) -> &Probe {
        return probes_definition
            .get(probes_placed.get(vid).unwrap_or_else(|| {
                // println!(
                //     "get_probe_from_vid: probes_placed {:#?} doesnt have {:#?}",
                //     probes_placed, vid
                // );
                &u16::MAX
            }))
            .unwrap_or_else(|| {
                // println!(
                //     "get_probe_from_vid: probes_definition {:#?} doesnt have {:#?}",
                //     probes_definition, vid
                // );
                &B
            });
    }

    fn get_boost_from_boosters(
        &self,
        vid: &VId,
        probes_placed: &FnvHashMap<VId, u16>,
        probes_definition: &FnvHashMap<u16, Probe>,
    ) -> f32 {
        self.edges.get(&vid).unwrap().iter().fold(1.0, |acc, curr| {
            let p = self.get_probe_from_vid(curr, probes_placed, probes_definition);
            if p.probe_type != ProbeType::BOOST {
                return acc;
            }
            acc * p.boost
        })
    }

    fn get_boost_from_duplicators(
        &self,
        vid: &VId,
        probes_placed: &FnvHashMap<VId, u16>,
        probes_definition: &FnvHashMap<u16, Probe>,
    ) -> f32 {
        self.edges.get(&vid).unwrap().iter().fold(1.0, |acc, curr| {
            let p = self.get_probe_from_vid(curr, probes_placed, probes_definition);
            if p.probe_type != ProbeType::DUPLICATOR {
                return acc;
            }
            acc * self.get_boost_from_boosters(curr, probes_placed, probes_definition)
        })
    }

    // TODO: Implement highest revenue
}

pub fn get_hash<VId: Debug + Display + Ord>(
    probes_placed: &FnvHashMap<VId, u16>,
    probes_definition: &FnvHashMap<u16, Probe>,
) -> String {
    let mut aux: Vec<(&VId, &u16)> = probes_placed.iter().collect();
    aux.sort_by(|a, b| b.0.cmp(a.0));
    return aux.iter().fold("".to_string(), |acc, p| {
        // println!("Hashing: {:#?} {:#?}", p.0, p.1);
        let curr_probe = probes_definition.get(p.1).unwrap_or_else(|| {
            // println!("No probe for {:#?} in {:#?}", p.1, probes_definition);
            &MG1
        });
        let curr_probe_id = curr_probe.id;
        let curr_node_id = p.0;
        return format!("{curr_node_id}:{curr_probe_id},{acc}");
    });
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::{get_hash, Graph};
    use crate::{
        tests::common::graph_utils::create_graph,
        v2::{
            constants::{BG2, DG1, MG1, MG2},
            graph::Probe,
            vertex::Vertex,
        },
    };
    use fnv::FnvHashMap;

    #[test]
    fn find_biggest_cluster_test() {
        let graph = create_graph();

        let expect = graph.0.find_biggest_cluster();

        assert_eq!(4, expect.len());
    }

    #[test]
    fn find_highest_production() {
        let (graph, _, probes_definition) = create_graph();
        let probes_available: [u16; 4] = [1,2,3,4];
        let mut combinations_checked: HashMap<String, u16> = HashMap::new();

        let expected = graph.find_highest_production(
            'M',
            FnvHashMap::default(),
            &probes_available,
            &probes_definition,
            &mut combinations_checked,
        );
        println!("Solution: {:#?}", expected);

        assert_eq!(5595.0, expected.0);
    }

    #[test]
    fn copy_adjacent_productions_test() {
        let graph = create_graph();

        let expected = 4.3;
        let actual = graph
            .0
            .copy_adjacent_productions(&"100", &graph.1, &graph.2);

        assert_eq!(expected, actual);
    }

    #[test]
    fn total_production_of_mg1_and_mg1_test() {
        let mut graph: Graph<&str, Vertex> = Graph::new();
        graph.push_vertex(
            "100",
            Vertex {
                id: String::from("100"),
                production: 'B',
                revenue: 'F',
            },
        );
        graph.push_vertex(
            "101",
            Vertex {
                id: String::from("101"),
                production: 'C',
                revenue: 'E',
            },
        );
        graph.push_edge("100", "101");
        let mut probes_placed: FnvHashMap<&str, u16> = FnvHashMap::default();
        probes_placed.insert("100", 1);
        probes_placed.insert("101", 2);
        let mut probes_definition: FnvHashMap<u16, Probe> = FnvHashMap::default();
        probes_definition.insert(1, MG1);
        probes_definition.insert(2, MG1);

        let expected = 350.0 * 1.0 + 250.0 * 1.0;
        let actual = graph.total_production('M', &probes_placed, &probes_definition);

        assert_eq!(expected, actual);
    }

    #[test]
    fn total_production_test() {
        let (graph, probes_placed, probes_definition) = create_graph();

        let expected = 1.0;
        let actual = graph.total_production('M', &probes_placed, &probes_definition);

        assert_eq!(expected, actual);
    }

    #[test]
    fn find_highest_production_deep_dive_test() {
        let mut graph: Graph<&str, Vertex> = Graph::new();
        graph.push_vertex(
            "v1",
            Vertex {
                id: String::from("1"),
                production: 'A',
                revenue: 'A',
            },
        );
        graph.push_vertex(
            "v2",
            Vertex {
                id: String::from("2"),
                production: 'B',
                revenue: 'B',
            },
        );
        graph.push_vertex(
            "v3",
            Vertex {
                id: String::from("3"),
                production: 'C',
                revenue: 'C',
            },
        );
        graph.push_vertex(
            "v4",
            Vertex {
                id: String::from("4"),
                production: 'C',
                revenue: 'D',
            },
        );
        graph.push_edge("v1", "v2");
        graph.push_edge("v2", "v1");
        graph.push_edge("v1", "v3");
        graph.push_edge("v3", "v1");
        graph.push_edge("v2", "v3");
        graph.push_edge("v3", "v2");
        graph.push_edge("v3", "v4");
        graph.push_edge("v4", "v3");
        let probes_placed = FnvHashMap::default();
        let probes_available = vec![1001, 1002];
        let mut probes_definition: FnvHashMap<u16, Probe> = FnvHashMap::default();
        probes_definition.insert(1001, MG1);
        probes_definition.insert(1002, DG1);
        let mut combinations_checked: HashMap<String, u16> = HashMap::new();

        // DG1 in 102/v3 and MG1 in 100/v1
        let expected = (500.0 * 1.0) + (350.0 * 1.0) + (250.0 * 0.5) + (250.0 * 0.5);
        let actual = graph.find_highest_production_subgraph(
            'M',
            probes_placed,
            &probes_available,
            &probes_definition,
            &mut combinations_checked,
        );

        assert_eq!(expected, actual.0);
    }

    #[test]
    fn find_highest_production_subgraph_branch_purge_test() {
        let mut graph: Graph<&str, Vertex> = Graph::new();
        graph.push_vertex(
            "v1",
            Vertex {
                id: String::from("1"),
                production: 'A',
                revenue: 'A',
            },
        );
        graph.push_vertex(
            "v2",
            Vertex {
                id: String::from("2"),
                production: 'B',
                revenue: 'B',
            },
        );
        graph.push_vertex(
            "v3",
            Vertex {
                id: String::from("3"),
                production: 'C',
                revenue: 'C',
            },
        );
        graph.push_vertex(
            "v4",
            Vertex {
                id: String::from("4"),
                production: 'D',
                revenue: 'D',
            },
        );
        graph.push_edge("v1", "v2");
        graph.push_edge("v1", "v3");
        graph.push_edge("v2", "v3");
        let placed_probes = FnvHashMap::default();
        let probes_available = vec![1001, 1002];
        let mut probes: FnvHashMap<u16, Probe> = FnvHashMap::default();
        probes.insert(1001, MG1);
        probes.insert(1002, MG1);
        let mut combinations_checked: HashMap<String, u16> = HashMap::new();

        let actual = graph.find_highest_production_subgraph(
            'M',
            placed_probes,
            &probes_available,
            &probes,
            &mut combinations_checked,
        );

        assert_eq!(6.0, actual.0);
    }

    #[test]
    fn find_highest_production_subgraph_each_type_placed_test() {
        let (graph, probes_placed, probes_definition) = create_graph();
        let probes_available = [1, 2, 3, 4];
        let mut combinations_checked: HashMap<String, u16> = HashMap::new();

        let actual = graph.find_highest_production_subgraph(
            'M',
            probes_placed,
            &probes_available,
            &probes_definition,
            &mut combinations_checked,
        );
        println!("Best solution: {:#?}", actual);

        assert_eq!(4500.0, actual.0);
    }

    #[test]
    fn total_production_of_base_case() {
        let (graph, _, probes_definition) = create_graph();
        let mut probes_placed: FnvHashMap<&str, u16> = FnvHashMap::default();
        probes_placed.insert("100", 1);
        probes_placed.insert("101", 3);
        probes_placed.insert("102", 2);
        probes_placed.insert("103", 4);

        let expected = 5595.0;
        let actual = graph.total_production('M', &probes_placed, &probes_definition);

        assert_eq!(expected, actual);
    }

    #[test]
    fn find_highest_production_subgraph_base_case_only_duplicators() {
        let (graph, _, probes_definition) = create_graph();
        let probes_placed = FnvHashMap::default();
        let probes_available = [1];
        let mut combinations_checked: HashMap<String, u16> = HashMap::new();

        let actual = graph.find_highest_production_subgraph(
            'M',
            probes_placed,
            &probes_available,
            &probes_definition,
            &mut combinations_checked,
        );
        println!("Best solution: {:#?}", actual);

        assert_eq!(4500.0, actual.0);
    }

    #[test]
    fn find_highest_production_subgraph_each_type_test() {
        let (graph, _, probes_definition) = create_graph();
        let probes_available = [1, 2, 3, 4];
        let mut combinations_checked: HashMap<String, u16> = HashMap::new();

        let actual = graph.find_highest_production_subgraph(
            'M',
            FnvHashMap::default(),
            &probes_available,
            &probes_definition,
            &mut combinations_checked,
        );
        println!("Best solution: {:#?}", actual);

        assert_eq!(4500.0, actual.0);
    }

    #[test]
    fn get_hash_test() {
        let mut probes: FnvHashMap<u16, Probe> = FnvHashMap::default();
        probes.insert(0, MG1);
        probes.insert(1, DG1);
        probes.insert(2, DG1);
        probes.insert(3, MG2);
        probes.insert(4, BG2);
        let mut current_state: FnvHashMap<&str, u16> = FnvHashMap::default();
        current_state.insert("101", 1);
        current_state.insert("103", 3);
        current_state.insert("102", 2);
        current_state.insert("104", 4);
        current_state.insert("100", 0);

        let expected = "100:1,101:23,102:23,103:2,104:22,";
        let actual = get_hash::<&str>(&current_state, &probes);

        assert_eq!(expected, actual);
    }
}
