use std::{collections::HashMap, fs};

use super::{
    constants::{parse_probe_grade, parse_probe_type, ProbeGrade, ProbeType, PROBE_SET},
    graph::Graph,
    probe::Probe,
    vertex::Vertex,
};

/// Reads file
///
/// Returns a graph
pub fn build_graph_from_file(file_path: &str) -> Graph<String, Vertex> {
    let mut graph = Graph::new();

    // Read file
    let contents = fs::read_to_string(file_path).expect("Should have been able to read the file");

    // Split file into vertices and edges
    let (vertices, edges) = contents.split_at(contents.find("edges").unwrap());

    // Parse vertices
    vertices
        .lines()
        .skip(1)
        .filter(|line: &&str| !line.is_empty() && !line.starts_with("//"))
        .for_each(|vertex_str| {
            let v = parse_vertex(vertex_str);

            graph.push_vertex(
                v.id.to_owned(),
                v
            );
        });

    edges
        .lines()
        .skip(1)
        .filter(|line| !line.is_empty() && !line.starts_with("//"))
        .for_each(|edge_str| {
            let edge = parse_edge(edge_str);
            graph.push_edge(edge.0, edge.1);
        });

    // println!("Built graph: {:?}", graph);

    return graph;
}

fn parse_vertex(vertex: &str) -> Vertex {
    let vertex_fields: Vec<&str> = vertex.split(",").collect();
    let production: char = vertex_fields[1].chars().next().unwrap_or('C');
    let revenue: char = vertex_fields[2].chars().next().unwrap_or('F');
    Vertex {
        id: vertex_fields[0].to_string(),
        production,
        revenue,
    }
}

fn parse_edge(edge: &str) -> (String, String) {
    let (from, to) = edge.split_once(",").unwrap();

    (from.to_string(), to.to_string())
}

// /// Reads file
// ///
// /// Returns a vector of Probes
pub fn build_probes_from_file(file_path: &str) -> HashMap<u16, Probe> {
    let contents: String =
        fs::read_to_string(file_path).expect("Should have been able to read the file");

    let mut output: HashMap<u16, Probe> = HashMap::new();
    contents
        .lines()
        .filter(|line| !line.is_empty() && !line.starts_with("//"))
        .enumerate()
        .for_each(|(i, line)| {
            output.insert(i.try_into().unwrap(), parse_probe(line));
        });
    return output;
}

// /// Parses Probe struct from a text
fn parse_probe(line: &str) -> Probe {
    // println!("Probe: {:?}", line);
    let (probe_type, probe_grade) = line
        .split_once("G")
        .unwrap_or_else(|| panic!("Invalid probe line"));
    let probe_type: ProbeType = parse_probe_type(probe_type);
    let probe_grade: ProbeGrade = parse_probe_grade(probe_grade);
    return PROBE_SET
        .iter()
        .find(|p| {
            // println!(
            //     "Checking...{:?} == {:?}, {:?} == {:?}",
            //     p.probe_type, probe_type, p.probe_grade, probe_grade
            // );
            return p.probe_type == probe_type && p.probe_grade == probe_grade;
        })
        .unwrap_or_else(|| panic!("Fuck this this"))
        .to_owned();
}

#[cfg(test)]
mod tests {

    use super::*;

    //     #[test]
    //     fn parse_vertex_test() {
    //         let actual: Vertex = parse_vertex("100,A,A");

    //         assert_eq!("FN100", format!("FN{}", actual.id));
    //         assert_eq!('A', actual.production);
    //         assert_eq!('A', actual.revenue);
    //         assert_eq!(ProbeType::BASIC, actual.probe.probe_type);
    //         assert_eq!(ProbeGrade::I, actual.probe.probe_grade);
    //         assert_eq!(0.5, actual.probe.production);
    //         assert_eq!(0.5, actual.probe.revenue);
    //     }

    //     #[test]
    //     fn parse_edge_test() {
    //         let actual: Edge = parse_edge("1,2");

    //         assert_eq!(1, actual.from);
    //         assert_eq!(2, actual.to);
    //     }

    //     #[test]
    //     fn build_graph_from_file_1_test() {
    //         let actual = build_graph_from_file("examples/graph-1.txt");

    //         assert_eq!(3, actual.vertices.len());
    //         assert_eq!(2, actual.edges.len());
    //         assert_eq!("FN100", format!("FN{}", actual.vertices[0].id));
    //         assert_eq!("FN101", format!("FN{}", actual.vertices[1].id));
    //         assert_eq!("FN102", format!("FN{}", actual.vertices[2].id));
    //         assert_eq!('A', actual.vertices[0].production);
    //         assert_eq!('F', actual.vertices[0].revenue);

    //         assert_eq!('C', actual.vertices[1].production);
    //         assert_eq!('E', actual.vertices[1].revenue);

    //         assert_eq!('B', actual.vertices[2].production);
    //         assert_eq!('D', actual.vertices[2].revenue);
    //     }

    #[test]
    fn build_graph_from_file_primordia_test() {
        let actual = build_graph_from_file("examples/graph-primordia.txt");
        print!("{:#?}", actual);

        assert_eq!(20, actual.vertices.len());
        assert_eq!(20, actual.edges.len());
    }

    //     #[test]
    //     fn parse_probe_test() {
    //         let expected = Probe {
    //             id: 1,
    //             probe_type: ProbeType::MINING,
    //             probe_grade: crate::v1::constants::ProbeGrade::I,
    //             production: 1.0,
    //             revenue: 0.3,
    //             boost: 0.0,
    //         };
    //         let actual = parse_probe("MG1");

    //         assert_eq!(actual.probe_type, expected.probe_type);
    //         assert_eq!(actual.production, expected.production);
    //         assert_eq!(actual.revenue, expected.revenue);
    //     }

    //     #[test]
    //     fn parse_probe_bg2_test() {
    //         let expected = Probe {
    //             id: 22,
    //             probe_type: ProbeType::BOOST,
    //             probe_grade: crate::v1::constants::ProbeGrade::II,
    //             production: 0.3,
    //             revenue: 0.3,
    //             boost: 2.0,
    //         };
    //         let actual = parse_probe("BG2");

    //         assert_eq!(actual.probe_type, expected.probe_type);
    //         assert_eq!(actual.production, expected.production);
    //         assert_eq!(actual.revenue, expected.revenue);
    //     }

    //     #[test]
    //     fn build_probes_from_file_1_test() {
    //         let actual: Vec<Probe> = build_probes_from_file("examples/probes-1.txt");

    //         assert_eq!(2, actual.len());
    //         assert_eq!(ProbeType::MINING, actual[0].probe_type);
    //         assert_eq!(ProbeGrade::I, actual[0].probe_grade);
    //         assert_eq!(ProbeType::MINING, actual[1].probe_type);
    //         assert_eq!(ProbeGrade::II, actual[1].probe_grade);
    //     }

    //     #[test]
    //     fn build_probes_from_file_2_test() {
    //         let actual: Vec<Probe> = build_probes_from_file("examples/probes-2.txt");

    //         assert_eq!(4, actual.len());
    //         assert_eq!(ProbeType::MINING, actual[0].probe_type);
    //         assert_eq!(ProbeGrade::I, actual[0].probe_grade);
    //         assert_eq!(ProbeType::MINING, actual[1].probe_type);
    //         assert_eq!(ProbeGrade::II, actual[1].probe_grade);
    //         assert_eq!(ProbeType::REVENUE, actual[2].probe_type);
    //         assert_eq!(ProbeGrade::X, actual[2].probe_grade);
    //         assert_eq!(ProbeType::BOOST, actual[3].probe_type);
    //         assert_eq!(ProbeGrade::I, actual[3].probe_grade);
    //     }
}
