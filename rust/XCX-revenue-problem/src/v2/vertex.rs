
pub trait VertexTrait {
    fn get_id(&self) -> &String;
    fn get_production(&self) -> char;
}

#[derive(Clone, Debug)]
pub struct Vertex {
    pub id: String, // Unique. Should be Box<str>?
    pub production: char,
    pub revenue: char,
}
impl VertexTrait for Vertex {
    fn get_id(&self) -> &String {
        return &self.id;
    }
    fn get_production(&self) -> char {
        return self.production;
    }
}
