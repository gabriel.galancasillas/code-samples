package com.ggc.hotelsearch.api.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ggc.hotelsearch.api.dto.SearchRequestDto;

@ExtendWith(MockitoExtension.class)
public class CheckInValidatorTest {

    @InjectMocks
    private CheckInValidator checkInValidator;

    @Test
    public void isValid_whenCheckInLTCheckout_test() {
        SearchRequestDto value = new SearchRequestDto("hotelId", new Date(1L), new Date(2L), List.of());

        boolean actual = checkInValidator.isValid(value, null);

        assertTrue(actual);
    }

    @Test
    public void isNotValid_whenCheckInGTCheckOut_test() {
        SearchRequestDto value = new SearchRequestDto("hotelId", new Date(2L), new Date(1L), List.of());

        boolean actual = checkInValidator.isValid(value, null);

        assertFalse(actual);
    }
}