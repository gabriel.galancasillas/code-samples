package com.ggc.hotelsearch.api.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.ggc.hotelsearch.api.dto.SearchCompleteResponseDto;
import com.ggc.hotelsearch.api.dto.SearchResponseDto;
import com.ggc.hotelsearch.api.exception.ErrorResponse;

import reactor.core.publisher.Mono;

@ActiveProfiles("test")
@AutoConfigureWebTestClient
@SpringBootTest
@EmbeddedKafka(partitions = 1, brokerProperties = { "listeners=PLAINTEXT://localhost:9092", "port=9092" })
public class SearchControllerTest {

    private final WebTestClient webTestClient;

    @Autowired
    SearchControllerTest(WebTestClient webTestClient) {
        this.webTestClient = webTestClient;
    }

    @Test
    void newSearch_happyPath_test() {
        String body = "{\"hotelId\":\"000000000000000000000001\",\"checkIn\":\"01/01/2023\",\"checkOut\":\"17/01/2023\",\"ages\":[33,34,83]}";

        SearchResponseDto actual = webTestClient.post()
                .uri("/search")
                .contentType(APPLICATION_JSON)
                .body(Mono.just(body), String.class)
                .exchange()
                .expectStatus().isCreated()
                .returnResult(SearchResponseDto.class)
                .getResponseBody()
                .blockFirst();

        assertNotNull(actual);
        assertNotNull(actual.getSearchId());
        assertEquals(24, actual.getSearchId().length());
    }

    @Test
    void newSearch_checkInAfterCheckOut_test() {
        String body = "{\"hotelId\":\"000000000000000000000001\",\"checkIn\":\"15/01/2023\",\"checkOut\":\"01/01/2023\",\"ages\":[33,34,83]}";

        ErrorResponse actual = webTestClient.post()
                .uri("/search")
                .contentType(APPLICATION_JSON)
                .body(Mono.just(body), String.class)
                .exchange()
                .expectStatus().isBadRequest()
                .returnResult(ErrorResponse.class)
                .getResponseBody()
                .blockFirst();

        assertNotNull(actual);
        assertNotNull(actual.getMessage());
        assertTrue(actual.getMessage().contains("Validation failed for argument at index 0"));
    }

    @Test
    public void count_test() {
        SearchCompleteResponseDto actual = webTestClient.get()
                .uri("/count?searchId=000000000000000000000001")
                .exchange()
                .expectStatus().isOk()
                .returnResult(SearchCompleteResponseDto.class)
                .getResponseBody()
                .blockFirst();

        assertNotNull(actual);
        assertNotNull(actual.getSearch());
        assertEquals(2, actual.getCount());

    }

}
