package com.ggc.hotelsearch.api.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SearchDtoTest {

    @Test
    public void getHash_test() {
        SearchDto searchDto = new SearchDto();
        searchDto.setHotelId("000000000000000000000001");
        searchDto.setCheckIn(new Date(1L));
        searchDto.setCheckOut(new Date(2L));
        searchDto.setAges(List.of(3, 2, 1));

        String actual = searchDto.getHash();

        assertEquals("MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAxMDEvMDEvMTk3MDAxLzAxLzE5NzAxLDIsMw==", actual);
    }

    @Test
    public void getHash_effectivelyEqualSearches_test() {
        SearchDto searchDtoA = new SearchDto();
        searchDtoA.setHotelId("000000000000000000000001");
        searchDtoA.setCheckIn(new Date(1000L));
        searchDtoA.setCheckOut(new Date(2000L));
        searchDtoA.setAges(List.of(3, 2, 1));

        SearchDto searchDtoB = new SearchDto();
        searchDtoB.setHotelId("000000000000000000000001");
        searchDtoB.setCheckIn(new Date(1001L));
        searchDtoB.setCheckOut(new Date(2001L));
        searchDtoB.setAges(List.of(1, 3, 2));

        String actualA = searchDtoA.getHash();
        String actualB = searchDtoB.getHash();

        assertEquals(actualA, actualB);

    }
}