package com.ggc.hotelsearch.domain.repository.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.ggc.hotelsearch.domain.repository.SearchRepository;

import reactor.test.StepVerifier;

@SpringBootTest
@ActiveProfiles("test")
public class SearchRepositoryImplTest {

    @Autowired
    SearchRepository searchRepository;

    @Test
    void count_happyPath_test() {
        var id = new ObjectId("000000000000000000000001");

        StepVerifier.create(searchRepository.count(id.toHexString()))
                .assertNext(actual -> {
                    assertEquals(2, actual.getCount());
                    assertEquals("01", actual.getSearchHash());
                    assertEquals("000000000000000000000001", actual.getSearchId().toHexString());
                    assertEquals("100000000000000000000001", actual.getHotelId().toHexString());
                })
                .verifyComplete();
    }

    @Test
    void count_shouldReturnRightElement_test() {
        var id = new ObjectId("000000000000000000000002");

        StepVerifier.create(searchRepository.count(id.toHexString()))
                .assertNext(actual -> {
                    assertEquals(2, actual.getCount());
                    assertEquals("02", actual.getSearchHash());
                    assertEquals("100000000000000000000002", actual.getHotelId().toHexString());
                    assertEquals("000000000000000000000002", actual.getSearchId().toHexString());
                })
                .verifyComplete();
    }

    @Test
    void count_documentNotFound_test() {
        var id = new ObjectId("00000000000000000000000F");

        StepVerifier.create(searchRepository.count(id.toHexString()))
                .expectComplete();
    }
}
