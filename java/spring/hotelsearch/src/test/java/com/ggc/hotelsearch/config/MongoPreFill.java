package com.ggc.hotelsearch.config;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import reactor.core.publisher.Mono;

@Configuration
@AutoConfigureAfter(value = { MongoAutoConfiguration.class, MongoDataAutoConfiguration.class })
public class MongoPreFill {

    private static final String Z = ".000Z";
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    Logger logger = LoggerFactory.getLogger(MongoPreFill.class);

    @Bean
    public boolean insertDocumentsFromMongoExtendedJsonFile(ReactiveMongoTemplate mongoTemplate) {
        mongoExtendedJsonFilesLookup().forEach((collectionName, path) -> {
            try {
                List<Document> documents = new ArrayList<>();
                Files.readAllLines(path)
                        .stream().filter(line -> !line.startsWith("#"))
                        .forEach(l -> {
                            Document document = Document.parse(l
                                    .replace("${NOW}", getNow())
                                    .replace("${TOMORROW}", getTomorrow())
                                    .replace("${YESTERDAY}", getYesterday()));
                            documents.add(document);
                        });
                mongoTemplate.getCollection(collectionName).flatMap(x -> Mono.from(x.insertMany(documents)))
                        .block();
            } catch (Exception ex) {
                logger.error(path.toAbsolutePath().toString(), ex);
            }
        });
        return true;
    }

    /**
     * Just in case if the DB test dataset needed to be parameterizable
     */
    private String getNow() {
        Calendar calendar = Calendar.getInstance();
        return new SimpleDateFormat(DATE_FORMAT).format(calendar.getTime())
                .replace(' ', 'T').concat(Z);
    }

    /**
     * Just in case if the DB test dataset needed to be parameterizable
     */
    private String getTomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        return new SimpleDateFormat(DATE_FORMAT).format(calendar.getTime())
                .replace(' ', 'T').concat(Z);
    }

    /**
     * Just in case if the DB test dataset needed to be parameterizable
     */
    private String getYesterday() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return new SimpleDateFormat(DATE_FORMAT).format(calendar.getTime())
                .replace(' ', 'T').concat(Z);
    }

    private Map<String, Path> mongoExtendedJsonFilesLookup() {
        Map<String, Path> collections = new HashMap<>();
        try (Stream<Path> x = Files.walk(Paths.get("src", "test", "resources", "mongo"))) {
            x.filter(Files::isRegularFile)
                    .forEach(filePath -> collections.put(
                            filePath.getFileName().toString().replace(".json", ""),
                            filePath));
        } catch (Exception ex) {
            logger.error("Error reading path", ex);
        }
        return collections;
    }

}
