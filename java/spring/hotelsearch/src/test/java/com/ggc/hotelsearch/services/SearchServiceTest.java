package com.ggc.hotelsearch.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;

import com.ggc.hotelsearch.api.dto.SearchDto;
import com.ggc.hotelsearch.domain.entity.Search;
import com.ggc.hotelsearch.domain.producer.HotelAvailabilitySearchProducer;
import com.ggc.hotelsearch.domain.repository.SearchRepository;
import com.ggc.hotelsearch.mappers.SearchMapper;
import com.ggc.hotelsearch.services.exception.InvalidSearchDtoException;
import com.ggc.hotelsearch.services.impl.SearchServiceImpl;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
public class SearchServiceTest {

    @Mock
    SearchRepository searchRepository;

    @Mock
    HotelAvailabilitySearchProducer hotelAvailabilitySearchProducer;

    @Mock
    SearchMapper searchMapper;

    @InjectMocks
    SearchServiceImpl searchService;

    @Test
    public void sendSearch_failsOnJsonMappingError_test() {
        SearchDto searchDto = new SearchDto();
        when(searchMapper.toJson(searchDto)).thenReturn(null);

        assertThrows(InvalidSearchDtoException.class, () -> {
            searchService.sendSearch(searchDto);
        });
    }

    @Test
    public void sendSearch_happyPath_test() {
        String searchId = "sendSearch";
        SearchDto searchDto = new SearchDto();
        when(searchMapper.toJson(searchDto)).thenReturn(searchId);
        when(hotelAvailabilitySearchProducer.send(anyString(), eq(searchId))).thenReturn(searchId);

        StepVerifier.create(searchService.sendSearch(searchDto))
                .assertNext(searchResponseDto -> {
                    assertNotNull(searchResponseDto);
                    assertEquals(searchId, searchResponseDto.getSearchId());
                });
    }

    @Test
    public void saveSearch_happyPath_test() {
        String searchId = "000000000000000000000001";
        SearchDto searchDto = new SearchDto();
        when(searchMapper.toDto(anyString())).thenReturn(searchDto);
        Search search = new Search();
        when(searchMapper.toEntity(searchDto)).thenReturn(search);
        when(searchRepository.save(search)).thenReturn(Mono.just(search));

        StepVerifier.create(searchService.saveSearch(searchId, anyString()))
                .assertNext(searchResponseDto2 -> {
                    assertEquals(searchId, searchResponseDto2.getSearchId());
                });
    }

    @Test
    public void count_shouldThrowErrorOnEmpty_test() {
        String searchId = "count";
        when(searchRepository.count(searchId)).thenReturn(Mono.empty());

        StepVerifier.create(searchService.count(searchId))
                .expectError(NotFoundException.class);
    }

}
