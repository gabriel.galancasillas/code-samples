package com.ggc.hotelsearch.mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ggc.hotelsearch.api.dto.SearchDto;
import com.ggc.hotelsearch.api.dto.SearchRequestDto;
import com.ggc.hotelsearch.domain.entity.Search;
import com.ggc.hotelsearch.mappers.impl.SearchMapperImpl;

@ExtendWith(MockitoExtension.class)
public class SearchMapperTest {

    @InjectMocks
    private SearchMapperImpl searchMapper;

    /* Downstream */

    @Test
    public void toDtoTest() {
        SearchRequestDto source = new SearchRequestDto("hotelId", new Date(), new Date(), List.of(1, 2, 3));

        var actual = searchMapper.toDto(source);

        assertNotNull(actual);
        assertEquals(source.hotelId(), actual.getHotelId());
        // ...
        assertEquals(source.ages(), actual.getAges());
    }

    @Test
    public void toEntity_test() {
        SearchDto source = new SearchDto();
        source.setHotelId("000000000000000000000001");
        source.setCheckIn(new Date(1L));
        source.setCheckOut(new Date(2L));
        source.setAges(List.of(3, 2, 1));

        var actual = searchMapper.toEntity(source);

        assertNotNull(actual);
        assertEquals(source.getHotelId(), actual.getHotelId().toHexString());
        assertEquals(source.getCheckIn(), actual.getCheckIn());
        assertEquals(source.getCheckOut(), actual.getCheckOut());
        assertEquals(source.getAges(), actual.getAges());
        assertEquals(source.getHash(), actual.getSearchHash());
    }

    @Test
    public void toJson_test() {
        SearchDto source = new SearchDto();
        source.setHotelId("000000000000000000000001");
        source.setCheckIn(new Date(1L));
        source.setCheckOut(new Date(2L));
        source.setAges(List.of(3, 2, 1));

        var actual = searchMapper.toJson(source);

        assertEquals("{\"hotelId\":\"000000000000000000000001\",\"checkIn\":1,\"checkOut\":2,\"ages\":[3,2,1]}",
                actual);
    }

    // /* Upstream */

    @Test
    public void toResponseDto_test() {
        Search source = new Search();
        source.setSearchId(new ObjectId());

        var actual = searchMapper.toResponseDto(source);

        assertEquals(source.getSearchId().toHexString(), actual.getSearchId());
    }

}
