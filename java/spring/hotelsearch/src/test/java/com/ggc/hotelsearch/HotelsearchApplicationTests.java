package com.ggc.hotelsearch;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class HotelsearchApplicationTests {

	@Test
	void contextLoads() {
	}

}
