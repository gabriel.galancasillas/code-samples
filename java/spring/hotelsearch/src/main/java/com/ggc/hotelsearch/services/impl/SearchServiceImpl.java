package com.ggc.hotelsearch.services.impl;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import com.ggc.hotelsearch.api.dto.SearchCompleteResponseDto;
import com.ggc.hotelsearch.api.dto.SearchDto;
import com.ggc.hotelsearch.api.dto.SearchResponseDto;
import com.ggc.hotelsearch.domain.producer.HotelAvailabilitySearchProducer;
import com.ggc.hotelsearch.domain.repository.SearchRepository;
import com.ggc.hotelsearch.mappers.SearchMapper;
import com.ggc.hotelsearch.services.SearchService;
import com.ggc.hotelsearch.services.exception.InvalidSearchDtoException;
import com.ggc.hotelsearch.services.exception.SendingEventFailedException;

import reactor.core.publisher.Mono;

@Service
public class SearchServiceImpl implements SearchService {

    private final SearchRepository searchRepository;

    private final HotelAvailabilitySearchProducer hotelAvailabilitySearchProducer;

    private final SearchMapper searchMapper;

    private final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);

    @Autowired
    public SearchServiceImpl(
            SearchRepository searchRepository,
            HotelAvailabilitySearchProducer hotelAvailabilitySearchProducer,
            SearchMapper searchMapper) {
        this.searchRepository = searchRepository;
        this.hotelAvailabilitySearchProducer = hotelAvailabilitySearchProducer;
        this.searchMapper = searchMapper;
    }

    @Override
    public Mono<SearchResponseDto> sendSearch(SearchDto searchDto) {
        var searchId = new ObjectId();

        var message = searchMapper.toJson(searchDto);
        if (message == null) {
            throw new InvalidSearchDtoException();
        }

        String confirmedSearchId = hotelAvailabilitySearchProducer.send(searchId.toHexString(), message);
        if (confirmedSearchId == null) {
            throw new SendingEventFailedException(searchId.toHexString());
        }

        var response = new SearchResponseDto();
        response.setSearchId(searchId);

        return Mono.just(response);
    }

    @Override
    public Mono<SearchResponseDto> saveSearch(String searchId, String searchString) {
        SearchDto searchDto = searchMapper.toDto(searchString);
        if (searchDto == null) {
            logger.error("Error deserializing message: {}", searchString);
            throw new InvalidSearchDtoException();
        }

        var search = searchMapper.toEntity(searchDto);
        search.setSearchId(new ObjectId(searchId));

        return searchRepository.save(search)
                .doOnNext(s -> {
                    logger.info("Search saved {}", s.getSearchId().toHexString());
                })
                .doOnError(e -> {
                    logger.error("Error saving search {}", searchDto.getHotelId());
                })
                .map(searchMapper::toResponseDto);
    }

    @Override
    public Mono<SearchCompleteResponseDto> count(String searchId) {
        return searchRepository.count(searchId)
                .switchIfEmpty(Mono.error(new NotFoundException()))
                .map(searchMapper::toCompleteResponseDto);
    }

}
