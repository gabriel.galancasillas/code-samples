package com.ggc.hotelsearch.api.exception;

import java.util.List;

public class ErrorResponse {

    long timestamp;

    public long getTimestamp() {
        return timestamp;
    }

    int status;

    public int getStatus() {
        return status;
    }

    String error;

    public String getError() {
        return error;
    }

    String exception;

    public String getException() {
        return exception;
    }

    String message;

    public String getMessage() {
        return message;
    }

    List<String> fieldsWithError;

    public List<String> getFieldsWithError() {
        return fieldsWithError;
    }

    /* Methods */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setFieldsWithError(List<String> fieldsWithError) {
        this.fieldsWithError = fieldsWithError;
    }
}
