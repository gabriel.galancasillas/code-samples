package com.ggc.hotelsearch.domain.repository.impl;

import org.apache.logging.log4j.util.Strings;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationExpression;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import com.ggc.hotelsearch.domain.entity.Search;
import com.ggc.hotelsearch.domain.entity.SearchAggregationCount;
import com.ggc.hotelsearch.domain.repository.SearchRepositoryCustom;
import com.ggc.hotelsearch.services.exception.InvalidSearchIdException;

import reactor.core.publisher.Mono;

public class SearchRepositoryImpl implements SearchRepositoryCustom {

    private final ReactiveMongoTemplate reactiveMongoTemplate;

    public SearchRepositoryImpl(
            ReactiveMongoTemplate reactiveMongoTemplate) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
    }

    @Override
    public Mono<SearchAggregationCount> count(String searchId) {
        if (Strings.isBlank(searchId)) {
            throw new InvalidSearchIdException(searchId);
        }
        MatchOperation matchOperation = Aggregation
                .match(new Criteria("_id").is(new ObjectId(searchId)));

        LookupOperation lookupOperation = Aggregation.lookup("searches", "searchHash", "searchHash", "matches");

        AggregationExpression sumOperation = ArrayOperators.Size.lengthOfArray("matches");
        ProjectionOperation projectionOperation = Aggregation
                .project("_id", "hotelId", "checkIn", "checkOut", "ages", "searchHash").and(sumOperation).as("count");

        Aggregation aggregation = Aggregation
                .newAggregation(matchOperation, lookupOperation, projectionOperation);

        return reactiveMongoTemplate
                .aggregate(aggregation, Search.class, SearchAggregationCount.class)
                .next();
    }

}
