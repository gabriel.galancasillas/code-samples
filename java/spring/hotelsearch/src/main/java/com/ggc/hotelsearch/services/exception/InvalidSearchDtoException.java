package com.ggc.hotelsearch.services.exception;

public class InvalidSearchDtoException extends RuntimeException {
    
  public InvalidSearchDtoException() {
    super("Could not process json");
  }
}
