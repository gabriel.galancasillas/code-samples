package com.ggc.hotelsearch.domain.producer;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.ggc.hotelsearch.domain.consumer.HotelAvailabilitySearchConsumer;

@Component
public class HotelAvailabilitySearchProducer {

    private final String topicName;

    private final KafkaTemplate<String, String> kafkaTemplate;

    Logger logger = LoggerFactory.getLogger(HotelAvailabilitySearchConsumer.class);

    @Autowired
    public HotelAvailabilitySearchProducer(
            @Value("${topic.name.producer}") String topicName,
            KafkaTemplate<String, String> kafkaTemplate) {
        this.topicName = topicName;
        this.kafkaTemplate = kafkaTemplate;
    }

    public String send(String searchId, String searchJson) {
        logger.debug("Payload sent: {}", searchJson);
        var eventSentFuture = kafkaTemplate.send(topicName, searchId, searchJson);

        try {
            var response = eventSentFuture.get(5, TimeUnit.SECONDS);
            logger.info("Event sent successfully: {}", response.getProducerRecord().toString());
            return response.getProducerRecord().key();
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            logger.error("Couln't send event {}. {}", searchId, e.getMessage());
            return null;
        }
    }

}