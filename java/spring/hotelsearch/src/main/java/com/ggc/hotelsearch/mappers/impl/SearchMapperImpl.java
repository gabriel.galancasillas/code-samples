package com.ggc.hotelsearch.mappers.impl;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ggc.hotelsearch.api.dto.SearchCompleteResponseDto;
import com.ggc.hotelsearch.api.dto.SearchDto;
import com.ggc.hotelsearch.api.dto.SearchRequestDto;
import com.ggc.hotelsearch.api.dto.SearchResponseDto;
import com.ggc.hotelsearch.domain.entity.Search;
import com.ggc.hotelsearch.domain.entity.SearchAggregationCount;
import com.ggc.hotelsearch.mappers.SearchMapper;

@Component
public class SearchMapperImpl implements SearchMapper {

    private final ObjectMapper objectMapper = new ObjectMapper();

    /* Downstream */

    @Override
    public SearchDto toDto(SearchRequestDto source) {
        if (source == null) {
            return null;
        }

        SearchDto search = new SearchDto();

        search.setHotelId(source.hotelId());
        search.setCheckIn(source.checkIn());
        search.setCheckOut(source.checkOut());
        search.setAges(source.ages());

        return search;
    }

    @Override
    public Search toEntity(SearchDto source) {
        if (source == null) {
            return null;
        }

        Search search = new Search();

        search.setHotelId(new ObjectId(source.getHotelId()));
        search.setCheckIn(source.getCheckIn());
        search.setCheckOut(source.getCheckOut());
        search.setAges(source.getAges());
        search.setSearchHash(source.getHash());

        return search;
    }

    public String toJson(SearchDto source) {
        try {
            return objectMapper.writeValueAsString(source);
        } catch (Exception e) {
            return null;
        }
    }

    /* Upstream */

    @Override
    public SearchResponseDto toResponseDto(Search source) {
        if (source == null) {
            return null;
        }

        SearchResponseDto searchResponseDto = new SearchResponseDto();

        searchResponseDto.setSearchId(source.getSearchId());

        return searchResponseDto;
    }

    @Override
    public SearchCompleteResponseDto toCompleteResponseDto(SearchAggregationCount source) {
        if (source == null) {
            return null;
        }

        SearchCompleteResponseDto searchCompleteResponseDto = new SearchCompleteResponseDto();

        searchCompleteResponseDto.setSearchId(source.getSearchId());
        searchCompleteResponseDto.setCount(source.getCount());

        SearchDto searchDto = new SearchDto();
        if (source.getHotelId() != null) {
            searchDto.setHotelId(source.getHotelId().toHexString());
        }
        searchDto.setCheckIn(source.getCheckIn());
        searchDto.setCheckOut(source.getCheckOut());
        searchDto.setAges(source.getAges());
        searchCompleteResponseDto.setSearch(searchDto);

        return searchCompleteResponseDto;
    }

    @Override
    public SearchDto toDto(String source) {
        try {
            return objectMapper.readValue(source, SearchDto.class);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

}
