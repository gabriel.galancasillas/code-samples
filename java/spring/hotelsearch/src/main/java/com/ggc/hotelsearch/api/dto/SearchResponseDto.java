package com.ggc.hotelsearch.api.dto;

import org.bson.types.ObjectId;

public class SearchResponseDto {
    ObjectId searchId;

    public String getSearchId() {
        return searchId.toHexString();
    }

    public void setSearchId(ObjectId searchId) {
        this.searchId = searchId;
    }
}