package com.ggc.hotelsearch.services;

import com.ggc.hotelsearch.api.dto.SearchCompleteResponseDto;
import com.ggc.hotelsearch.api.dto.SearchDto;
import com.ggc.hotelsearch.api.dto.SearchResponseDto;

import reactor.core.publisher.Mono;

public interface SearchService {

    public Mono<SearchResponseDto> sendSearch(SearchDto searchDto);

    public Mono<SearchResponseDto> saveSearch(String searchId, String searchJson);

    public Mono<SearchCompleteResponseDto> count(String searchId);

}
