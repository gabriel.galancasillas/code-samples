package com.ggc.hotelsearch.api.validator.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ggc.hotelsearch.api.validator.CheckInValidator;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CheckInValidator.class)
public @interface CheckInConstraint {
  String message() default "CheckIn must be previous to CheckOut";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};

}