package com.ggc.hotelsearch.services.exception;

public class InvalidSearchIdException extends RuntimeException {

  public InvalidSearchIdException(String value) {
    super("Invalid searchId: " + value);
  }
  
}
