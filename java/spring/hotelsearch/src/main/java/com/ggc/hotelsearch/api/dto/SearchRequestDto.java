package com.ggc.hotelsearch.api.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ggc.hotelsearch.api.validator.constraint.CheckInConstraint;

@CheckInConstraint
public record SearchRequestDto(
    @NotBlank
    String hotelId,

    @NotNull
    Date checkIn,

    @NotNull
    Date checkOut,

    @NotEmpty
    List<Integer> ages
) {
    public SearchRequestDto(
        String hotelId,
        @Pattern(regexp = "(0[1-9]|[12][0-9]|3[01])\\/(0[1-9]|1[1,2])\\/(19|20|21|22)\\d{2}")
        @JsonFormat(pattern="dd/MM/yyyy")
        Date checkIn,
        @Pattern(regexp = "(0[1-9]|[12][0-9]|3[01])\\/(0[1-9]|1[1,2])\\/(19|20|21|22)\\d{2}")
        @JsonFormat(pattern="dd/MM/yyyy")
        Date checkOut,
        List<Integer> ages
    ) {
        this.hotelId = hotelId;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.ages = ages;
    }

}
