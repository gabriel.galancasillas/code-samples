package com.ggc.hotelsearch.domain.entity;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document("searches")
public class SearchAggregationCount {

    @Id
    @MongoId
    @Field("searchId")
    ObjectId searchId;

    ObjectId hotelId;

    Date checkIn;

    Date checkOut;

    List<Integer> ages;

    String searchHash;

    Integer count;

    public ObjectId getSearchId() {
        return searchId;
    }

    public ObjectId getHotelId() {
        return hotelId;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public List<Integer> getAges() {
        return ages;
    }

    public String getSearchHash() {
        return searchHash;
    }

    public Integer getCount() {
        return count;
    }
}
