package com.ggc.hotelsearch.domain.entity;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document("searches")
public class Search {

    @Id
    @MongoId
    @Field("searchId")
    ObjectId searchId;

    ObjectId hotelId;

    Date checkIn;

    Date checkOut;

    List<Integer> ages;

    String searchHash;

    /* Getters & Setters */

    public void setSearchId(ObjectId searchId) {
        this.searchId = searchId;
    }
    public ObjectId getSearchId() {
        return searchId;
    }

    public void setHotelId(ObjectId hotelId) {
        this.hotelId = hotelId;
    }
    public ObjectId getHotelId() {
        return hotelId;
    }

    public Date getCheckIn() {
        return checkIn;
    }
    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }
    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public List<Integer> getAges() {
        return ages;
    }
    public void setAges(List<Integer> ages) {
        this.ages = ages;
    }

    public void setSearchHash(String searchHash) {
        this.searchHash = searchHash;
    }
    public String getSearchHash() {
        return searchHash;
    }
}
