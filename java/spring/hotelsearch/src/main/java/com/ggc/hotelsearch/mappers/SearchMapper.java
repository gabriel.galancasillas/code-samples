package com.ggc.hotelsearch.mappers;

import com.ggc.hotelsearch.api.dto.SearchCompleteResponseDto;
import com.ggc.hotelsearch.api.dto.SearchDto;
import com.ggc.hotelsearch.api.dto.SearchRequestDto;
import com.ggc.hotelsearch.api.dto.SearchResponseDto;
import com.ggc.hotelsearch.domain.entity.Search;
import com.ggc.hotelsearch.domain.entity.SearchAggregationCount;

public interface SearchMapper {

    /* Downstream */

    public SearchDto toDto(SearchRequestDto source);

    public Search toEntity(SearchDto source);

    public String toJson(SearchDto source);

    /* Upstream */

    public SearchResponseDto toResponseDto(Search search);

    public SearchCompleteResponseDto toCompleteResponseDto(SearchAggregationCount source);

    public SearchDto toDto(String source);

}
