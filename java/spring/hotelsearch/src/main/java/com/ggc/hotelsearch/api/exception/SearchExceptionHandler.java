package com.ggc.hotelsearch.api.exception;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebInputException;

@RestControllerAdvice
public class SearchExceptionHandler {

    Logger logger = LoggerFactory.getLogger(SearchExceptionHandler.class);

    /* Fallback for every un controlled exception */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable.class)
    public ErrorResponse handleException(Throwable ex) {
        logger.error("Generic error", ex.getMessage());
        return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
                ex.getClass().getName(),
                ex.getMessage(), List.of());
    }

    /* For javax validators */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(WebExchangeBindException.class)
    public ErrorResponse handleValidationException(WebExchangeBindException ex) {
        logger.error("Validation exception");
        var fieldsWithError = ex.getBindingResult().getAllErrors()
                .stream().map(error -> {
                    String fieldName = getField(error);
                    String errorMessage = error.getDefaultMessage();
                    return fieldName + " " + errorMessage;
                }).collect(Collectors.toList());
        return buildErrorResponse(HttpStatus.BAD_REQUEST, ex.getClass().getName(),
                ex.getMessage(), fieldsWithError);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServerWebInputException.class)
    public ErrorResponse handleValidationException(ServerWebInputException ex) {
        logger.error("Validation exception");
        String message = ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage();
        return buildErrorResponse(HttpStatus.BAD_REQUEST, ex.getClass().getName(),
                message, List.of());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ErrorResponse handleNotFoundException(NotFoundException ex) {
        String message = ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage();
        return buildErrorResponse(HttpStatus.NOT_FOUND, ex.getClass().getName(),
                message, List.of());
    }

    private String getField(ObjectError error) {
        try {
            return ((FieldError) error).getField();
        } catch (ClassCastException ex) {
            return "";
        }
    }

    private ErrorResponse buildErrorResponse(HttpStatus httpStatus, String name, String message,
            List<String> fieldsWithError) {
        ErrorResponse error = new ErrorResponse();
        error.setTimestamp(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        error.setStatus(httpStatus.value());
        error.setError(httpStatus.getReasonPhrase());
        error.setException(name);
        error.setMessage(message != null ? message : "no message");
        error.setFieldsWithError(fieldsWithError);

        return error;
    }

}
