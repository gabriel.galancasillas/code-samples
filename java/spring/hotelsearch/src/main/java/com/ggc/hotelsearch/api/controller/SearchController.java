package com.ggc.hotelsearch.api.controller;

import com.ggc.hotelsearch.api.dto.SearchCompleteResponseDto;
import com.ggc.hotelsearch.api.dto.SearchRequestDto;
import com.ggc.hotelsearch.api.dto.SearchResponseDto;

import reactor.core.publisher.Mono;

public interface SearchController {

    public Mono<SearchResponseDto> newSearch(SearchRequestDto searchRequestDto);

    public Mono<SearchCompleteResponseDto> count(String searchId);

}
