package com.ggc.hotelsearch.api.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ggc.hotelsearch.api.controller.impl.SearchControllerImpl;
import com.ggc.hotelsearch.api.dto.SearchRequestDto;
import com.ggc.hotelsearch.api.validator.constraint.CheckInConstraint;

public class CheckInValidator implements ConstraintValidator<CheckInConstraint, SearchRequestDto> {

  Logger logger = LoggerFactory.getLogger(SearchControllerImpl.class);

  @Override
  public boolean isValid(SearchRequestDto value, ConstraintValidatorContext context) {
    if (value.checkIn() == null || value.checkOut() == null) {
      return true;
    }

    return value.checkIn().before(value.checkOut());
  }
}