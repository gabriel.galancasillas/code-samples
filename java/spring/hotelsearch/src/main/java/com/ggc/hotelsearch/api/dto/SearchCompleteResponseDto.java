package com.ggc.hotelsearch.api.dto;

public class SearchCompleteResponseDto extends SearchResponseDto {

    SearchDto search;

    public SearchDto getSearch() {
        return search;
    }

    public void setSearch(SearchDto search) {
        this.search = search;
    }

    Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}