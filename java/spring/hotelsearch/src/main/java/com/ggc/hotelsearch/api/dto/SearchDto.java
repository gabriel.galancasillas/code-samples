package com.ggc.hotelsearch.api.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SearchDto {

    String hotelId;

    Date checkIn;

    Date checkOut;

    List<Integer> ages;

    /* Getters & Setters */

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public List<Integer> getAges() {
        return ages;
    }

    public void setAges(List<Integer> ages) {
        this.ages = ages;
    }

    /** 
     * Formatter used to create hash
     * This two dates acording to the api, should be considered the same date due to search input model
     * '2023-01-01T00:00:00.000Z'
     * '2023-01-01T00:01:00.000Z'
     * We need this two dates to be represented the same way for the hash function to output the same hash
     * It has many disclaimers
     */
    private static final SimpleDateFormat DateFormat = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Not actually a hash function because we could somehow decode it but naming is
     * hard.
     * Give me a break.
     * 
     * @return a String representing a search in a way different representations of
     *         the same search outputs the same value
     */
    @JsonIgnore
    public String getHash() {
        String checkInString = SearchDto.DateFormat.format(checkIn);
        String checkOutString = SearchDto.DateFormat.format(checkOut);
        var newAges = new ArrayList<>(ages);
        Collections.sort(newAges);
        var agesString = newAges.stream().map(a -> Integer.toString(a)).collect(Collectors.joining(","));
        var searchString = String.format("%s%s%s%s", hotelId, checkInString, checkOutString, agesString);
        return new String(Base64.getEncoder().encode(searchString.getBytes()));
    }

}