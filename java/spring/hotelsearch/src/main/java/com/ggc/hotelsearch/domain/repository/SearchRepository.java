package com.ggc.hotelsearch.domain.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.ggc.hotelsearch.domain.entity.Search;

public interface SearchRepository extends ReactiveMongoRepository<Search, ObjectId>, SearchRepositoryCustom {

}
