package com.ggc.hotelsearch.api.controller.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ggc.hotelsearch.api.controller.SearchController;
import com.ggc.hotelsearch.api.dto.SearchCompleteResponseDto;
import com.ggc.hotelsearch.api.dto.SearchRequestDto;
import com.ggc.hotelsearch.api.dto.SearchResponseDto;
import com.ggc.hotelsearch.mappers.SearchMapper;
import com.ggc.hotelsearch.services.SearchService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping
public class SearchControllerImpl implements SearchController {

    private final SearchService searchService;

    private final SearchMapper searchMapper;

    Logger logger = LoggerFactory.getLogger(SearchControllerImpl.class);

    @Autowired
    public SearchControllerImpl(
        SearchService searchService,
        SearchMapper searchMapper
    ) {
        this.searchService = searchService;
        this.searchMapper = searchMapper;
    }

    @Override
    @PostMapping("/search")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<SearchResponseDto> newSearch(@Validated @RequestBody SearchRequestDto searchRequestDto) {
        logger.info("new search sent");
        var result = searchService.sendSearch(searchMapper.toDto(searchRequestDto));
        logger.info("new search done");
        return result;
    }

    @Override
    @GetMapping("/count")
    public Mono<SearchCompleteResponseDto> count(@RequestParam("searchId") String searchId) {
        logger.info("count search sent");
        var result = searchService.count(searchId);
        logger.info("count search done");
        return result;
    }
}
