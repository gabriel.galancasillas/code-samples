package com.ggc.hotelsearch.domain.repository;

import com.ggc.hotelsearch.domain.entity.SearchAggregationCount;

import reactor.core.publisher.Mono;

public interface SearchRepositoryCustom {

    Mono<SearchAggregationCount> count(String searchId);

}
