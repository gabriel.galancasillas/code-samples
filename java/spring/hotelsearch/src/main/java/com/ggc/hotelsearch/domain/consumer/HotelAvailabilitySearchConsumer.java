package com.ggc.hotelsearch.domain.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.ggc.hotelsearch.services.SearchService;

@Service
public class HotelAvailabilitySearchConsumer {

    private final SearchService searchService;

    Logger logger = LoggerFactory.getLogger(HotelAvailabilitySearchConsumer.class);

    public HotelAvailabilitySearchConsumer(SearchService searchService) {
        this.searchService = searchService;
    }

    @KafkaListener(topics = "${topic.name.consumer}", groupId = "group_id")
    public void consume(ConsumerRecord<String, String> payload) {
        logger.info("Message received: topic: {}. key: {}. value: {}", payload.topic(), payload.key(), payload.value());
        searchService.saveSearch(payload.key(), payload.value()).subscribe();
    }

}