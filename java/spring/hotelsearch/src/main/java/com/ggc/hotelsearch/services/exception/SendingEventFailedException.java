package com.ggc.hotelsearch.services.exception;

public class SendingEventFailedException extends RuntimeException {

    public SendingEventFailedException(String value) {
        super("Event with key " + value + " could not be send");
    }

}
