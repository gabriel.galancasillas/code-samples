# TODO

- [x] POST MVP
- [x] Validators
- [x] Error handling
- [x] Hash function
- [x] Kafka listener
- [X] Create index on searchHash field (mongeez or mongobee?)
- [x] Ignore events from other topics
- [x] Unit tests
- [x] Mongo embedded in tests
- [x] Kafka embedded in tests
- [x] Feature tests
- [x] Docker for Kafka
- [X] Docker for Mongo
  - [ ] Mongo with credentials (DB, collection, user, pass)
- [ ] Case where kafka wasn't fast enough
- [x] Logs
- [x] Private license - By default

# File structure

─── src
└── main
├── java
│   └── com
│   └── ggc
│   └── hotelsearch
│   ├── api
│   │   ├── controller - HTTP REST controllers
│   │   ├── dto - service layer dtos
│   │   ├── exception - HTTP response exceptions and handler
│   │   └── validator - Request custom validators
│   ├── domain
│   │   ├── consumer - Kafka consumer (Listener)
│   │   ├── entity - DB entities
│   │   ├── producer - Kafka producer (Emitter)
│   │   └── repository - DB queries
│   ├── mappers - Transforms entities between layers
│   └── services - Connects framework layer (controllers) with domain layer (repositories and consumers/producers)
│   └── exception - Service custom exceptions
└── resources
└── application.yml - yml instead of .properties because of verbosity

# Mongo queries

```json
[
  {
    $match: {
      _id: ObjectId("64bef23e3b7b513c7dc8cbdc"),
    },
  },
  {
    $limit: 1,
  },
  {
    $lookup: {
      from: "searches",
      localField: "searchHash",
      foreignField: "searchHash",
      as: "matches",
    },
  },
  {
    $project: {
      _id: 1,
      hotelId: 1,
      checkIn: 1,
      checkOut: 1,
      ages: 1,
      searchHash: 1,
      count: {
        $size: "$matches",
      },
    },
  },
]
```

Disclaimer: Not sure an index over `searchHash` could be used
Check with `.explain()`
