# Prueba técnica Senior Software Engineer

# Objetivo

- Desarrollo de aplicación con Spring Boot.
- La aplicación debe proveer de dos llamadas REST:
- Llamada POST al path /search con la siguiente estructura:

```js
{
     "hotelId": "0000000000001",
     "checkIn": "01/12/2023",
     "checkOut": "15/12/2023",
     "ages": [21, 22, 25, 89]
}
```

- Devolverá un identificador atribuido a esta búsqueda:

```js
{
     "searchId": "fffffffffffff"
}
```

- La llamada a /search debe realizar las siguientes acciones:
- Validar el payload remitido
- El payload debe ser recibido en un objeto inmutable
- Debe enviarse ese payload al topic de Kafka `hotel_availability_searches`
- Se debe agregar un consumidor del topic de Kafka `hotel_availability_searches`` dónde recoja los mensajes y los persista en una base de datos (Mongo o Postgresql)

- Llamada GET al path /count con un único parámetro llamado searchId. Este parámetro contendrá el valor devuelto por la petición POST /search anterior y nos devolverá el número de búsquedas similares

```js
{
     "searchId": "fffffffffffff",
     "search": {
          "hotelId": "0000000000001",
          "checkIn": "01/12/2023",
          "checkOut": "15/12/2023",
          "ages": [21, 22, 25, 89]
     }
     "count": 100
}
```

Notas importantes

- No puede utilizarse Lombok
- La aplicación debe incluir tests.
