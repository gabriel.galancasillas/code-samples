# Blog

## Tabla de contenidos

- [Blog](#blog)
  - [Tabla de contenidos](#tabla-de-contenidos)
  - [Información](#información)
  - [Como ponerlo en marcha](#como-ponerlo-en-marcha)
  - [Páginas](#páginas)
  - [Estructura](#estructura)
  - [HTML](#html)
  - [CSS](#css)
    - [Clases como string en un fichero global](#clases-como-string-en-un-fichero-global)
    - [Clases como objeto de un módulo CSS generado por Gatsby](#clases-como-objeto-de-un-módulo-css-generado-por-gatsby)
    - [Responsive](#responsive)
  - [Como meter un post nuevo](#como-meter-un-post-nuevo)

## Información

|            |                                   |
| ---------- | --------------------------------- |
| Autor      | Gabriel Galán Casillas            |
| Fecha      | 2023/11/04                        |
| Asignatura | Desarrollo de Aplicaciones en Red |
| Sitio web  | https://ggc.github.io/            |

## Como ponerlo en marcha

Requisitos

-   Node.js +18.0.0

```shell
# Instalar dependencias
npm install
# Iniciar el entorno de desarrollo en http://localhost:8000
npm start
```

## Páginas

Blog (`/`): La homepage. Aquí se destacan posts

History (`/history`): Lista los posts ordenados descendientemente por fecha

Profile (`/profile`): Aquí hay información personal

Blog post (`/blog/<blogpost>`): Aquí se ve toda la información completa de un post. Hay blogs para `/blog/pizza`, `/blog/cooking` o `/blog/paint`. Uno por carpeta bajo `/blog`

## Estructura

-   **/blog**: Contiene los datos en crudo de los posts
    -   **\</blogpost>**
        -   **/image.jpg**: Imagen del blog
        -   **/index.mdx**: Fichero que contiene la información en mdx (pseudo markdown + frontmatter). Esta información se insertará en el datalayer de Gatsby para su posterior acceso con GraphQL
-   **/src**
    -   **/components**
        -   **/header**: Componente importado en el layout
        -   **/layout**: Plantilla usada en todas las páginas. Permite reusar código y mantener consistencia entre ellas
        -   **/seo**: Incluye en la cabecera información de la página en pro del posicionamiento en los buscadores web
    -   **/images**: Imágenes estáticas compartidas por varios htmls
        -   **/profile_default.jpg**: La imagen del header que es un link al perfil
    -   **/pages**
        -   **/404**: Página de error en caso de navegar a una URL no controlada [HTTP errors](https://es.wikipedia.org/wiki/HTTP_404)
        -   **/blog**: Contiene un template que permite el enrutado de Gatsby a través de su datalayer [Accessing the datalayer](https://www.gatsbyjs.com/docs/tutorial/getting-started/part-5/)
        -   **/history**: La página que tiene el listado de posts
-   **/package.json**: El fichero que define la configuración de un proyecto de Nodejs. Entre otras cosas contiene dependencias

## HTML

En este proyecto (basado en Reactjs), el HTML es el resultado de la compilación del código de reactjs.

Debido a esto, el código contiene etiquetas que no son elementos HTML, sino etiquetas de Reactjs. Estos elementos luego se transforman en conjuntos de elementos HTML con una configuración.

## CSS

### Clases como string en un fichero global

Funcionamiento del CSS de manual

```html
<div className="main"></div>
```

### Clases como objeto de un módulo CSS generado por Gatsby

profile.module.css

```css
.title {
    ...;
}
```

profile/index.js

```js
import { title } from "./profile.module.css"
[...]
<h1 className={title}>User</h1>
```

### Responsive

Se han usado mediaqueries para hacer (en la medida de lo posible) la web reponsive.

En la home se han usado mediaqueries para cambiar la altura de un componente y así ver en vez de horizontalmente, verticalmente los posts

```css
.headline__content__secondary {
    height: 11rem;
}

@media (max-width: 764px) {
    .headline__content__secondary {
        height: auto;
    }
}
```

## Como meter un post nuevo

En caso de querer meter un post nuevo (aunque no sea el propósito de la actividad) se podría hacer creando una carpeta en `/blog` de la misma manera que por ejemplo `/cooking` con su imagen y su `.mdx` editando este último.
