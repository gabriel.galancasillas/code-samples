import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import * as styles from './header.module.css'

const Header = ({ siteTitle }) => (
  <header
    className={styles.header}
  >
    <Link to="/" className={styles.header__entry}>Blog</Link>
    <Link to="/history" className={styles.header__entry}>History</Link>

    <Link to="/profile" className={styles.header__right} >
      <StaticImage
        className={styles.header__profile}
        alt="profile picture"
        src="../images/profile_default.jpg"
      />
    </Link>
  </header>
)

export default Header
