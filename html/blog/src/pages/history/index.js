import * as React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../../components/layout"
import Seo from "../../components/seo"
import * as styles from "./history.module.css"

const SecondPage = ({ data }) => {
    console.log("history: ", styles)

    return (
        <Layout>
            <div className={styles.main}>
                <hr />
                <h1 className={styles.title}>History</h1>
                <hr />

                <div className={styles.content}>
                    <ul className="history__content__list">
                        {data.allMdx.nodes.map(node => (
                            <Link key={node.id} to={`/blog/${node.frontmatter.slug}`}>
                                <article key={node.id} className={styles.post}>
                                    <h2 className={styles.postTitle}>
                                        {node.frontmatter.title}
                                    </h2>
                                    <p className={styles.postDate}>
                                        Posted: {node.frontmatter.date}
                                    </p>
                                    <p className={styles.postExcerpt}>
                                        {node.excerpt}
                                    </p>
                                </article>
                            </Link>
                        ))}
                    </ul>
                </div>
            </div>
        </Layout>
    )
}

export const query = graphql`
    query GetPostQuery {
        allMdx(sort: { frontmatter: { date: DESC } }) {
            nodes {
                id
                excerpt
                frontmatter {
                    date(formatString: "MMMM D, YYYY")
                    title
                    slug
                }
            }
        }
    }
`

export const Head = () => <Seo title="Page two" />

export default SecondPage
