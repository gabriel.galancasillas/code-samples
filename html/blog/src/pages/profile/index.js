import * as React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../../components/layout"
import Seo from "../../components/seo"
import { title } from "./profile.module.css"

const ProfilePage = () => {
  return (
    <Layout>
      <div className="main">
        <hr />
        <h1 className={title}>User</h1>
        <hr />

        <p>username: you@gmail.com</p>
        <p>password: passw0rd</p>
        <p>address: Fake street</p>
        <p>credit card number: 67890654321</p>

      </div>
    </Layout>
  )
}

export const Head = () => <Seo title="Profile" />

export default ProfilePage
