import { Link, graphql } from "gatsby"
import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"

import * as styles from './index.module.css'
import { GatsbyImage, getImage } from "gatsby-plugin-image"


const IndexPage = ({ data }) => (
    <Layout>
        <div className={styles.headline}>
            <hr className={styles.stickyBar}/>
            <h1 className={styles.headline__title}>blog</h1>
            <hr />

            <div className={styles.headline__content}>
                {data.allMdx.nodes.slice(0, 4).map((node, i) => {
                    const image = getImage(node.frontmatter.hero_image);
                    if (i == 0) {
                        return (
                            <Link to={`/blog/${node.frontmatter.slug}`}>
                                <article key={node.id} className={styles.headline__content__primary}>
                                    <GatsbyImage
                                        image={image}
                                        alt={node.frontmatter.hero_image_alt}
                                    ></GatsbyImage>

                                    <p className={styles.headline__content__date}>{node.frontmatter.date}</p>
                                    <h1 className={styles.headline__content__title}>{node.frontmatter.title}</h1>
                                    <p className={styles.headline__content__excerpt}>{node.excerpt}</p>
                                </article>
                            </Link>
                        )
                    }

                    return (
                        <Link to={`/blog/${node.frontmatter.slug}`} className={styles.headline__content__column}>

                            <article key={node.id} className={styles.headline__content__secondary}>
                                <GatsbyImage
                                    image={image}
                                    alt={node.frontmatter.hero_image_alt}
                                ></GatsbyImage>
                                <div className={styles.headline__content__secondaryText}>
                                    <p className={styles.headline__content__date}>{node.frontmatter.date}</p>
                                    <h1 className={styles.headline__content__title}>{node.frontmatter.title}</h1>
                                    <p className={styles.headline__content__excerpt}>{node.excerpt}</p>
                                </div>

                            </article>
                        </Link>
                    )
                })}

            </div>
        </div>
        <div className={styles.content}>
            {data.allMdx.nodes.slice(4).map(node => {
                const image = getImage(node.frontmatter.hero_image);
                return (
                    <Link to={`/blog/${node.frontmatter.slug}`}>
                        <article key={node.id} className={styles.headline__content__primary}>
                            <GatsbyImage
                                image={image}
                                alt={node.frontmatter.hero_image_alt}
                            ></GatsbyImage>

                            <p className={styles.headline__content__date}>{node.frontmatter.date}</p>
                            <h1 className={styles.headline__content__title}>{node.frontmatter.title}</h1>
                            <p className={styles.headline__content__excerpt}>{node.excerpt}</p>
                        </article>
                    </Link>
                )
            })}
        </div>
    </Layout>
)

export const query = graphql`
query GetPostsQuery {
    allMdx(sort: {frontmatter: {date: DESC}}, limit: 6) {
        nodes {
            excerpt
            id
            frontmatter {
                title
                date
                hero_image {
                    childImageSharp {
                        gatsbyImageData
                    }
                }
                hero_image_alt
                slug
            }
        }
    }
}`

export const Head = () => <Seo title="Home" />

export default IndexPage
