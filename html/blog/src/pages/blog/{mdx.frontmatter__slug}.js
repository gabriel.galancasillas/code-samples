import { graphql } from "gatsby";
import { GatsbyImage, getImage } from "gatsby-plugin-image";
import * as React from "react";
import Seo from "../../components/seo";
import Layout from "../../components/layout";
import { date } from "./{mdx.frontmatter__slug}.module.css";

const BlogPost = ({ data, children }) => {
    const image = getImage(data.mdx.frontmatter.hero_image);
    console.log("BlogPost: props: ", data, children);

    return (
        <Layout pageTitle={data.mdx.frontmatter.title}>
            <GatsbyImage
                image={image}
                alt={data.mdx.frontmatter.hero_image_alt}
            ></GatsbyImage>
            <p className={date}>{data.mdx.frontmatter.date}</p>
            <h1>{data.mdx.frontmatter.title}</h1>
            {children}
        </Layout>
    )
}

export const Head = ({ data }) => <Seo title={data.mdx.frontmatter.title} />

// TODO: How does this $id works?
export const query = graphql`
query GetPostQuery($id: String) {
    mdx(id: { eq: $id }) {
        frontmatter {
            title
            date(formatString: "MMMM D, YYYY")
            hero_image_alt
            hero_image_credit_link
            hero_image_credit_text
            hero_image {
                childImageSharp {
                    gatsbyImageData
                }
            }
        }
    }
}
`

export default BlogPost